/**
 * Created by acamargo on 09/02/2017.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2010/' + currentYear, function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });

    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });

}

function yearSelected() {

    $('#pieChartLoaderTotal').dimmer('show');
    $('#barChartLoaderTotal').dimmer('show');
    $('#pieChartLoaderEcco').dimmer('show');
    $('#pieChartLoaderEccp').dimmer('show');

    var year = $('#year').val();

    $.get('/api/coPiquet/RTU/total/pie/' + year, function(response) {
        var json = response

        drawPieChart(json, year, 'pieChartTotal', 'ECCO + ECCP')
        $('#pieChartLoaderTotal').dimmer('hide');

    })

    $.get('/api/coPiquet/RTU/ECCP/pie/' + year, function(response) {
        var json = response

        drawPieChart(json, year, 'pieChartEccp', 'ECCP')
        $('#pieChartLoaderEccp').dimmer('hide');


    })

    $.get('/api/coPiquet/RTU/ECCO/pie/' + year, function(response) {
        var json = response

        drawPieChart(json, year, 'pieChartEcco', 'ECCO')
        $('#pieChartLoaderEcco').dimmer('hide');

    })

    $.get('/api/coPiquet/RTU/total/bar/' + year, function(response) {
        var json = response

        drawBarChart(json, year, 'barChartTotal', 'Total')
        $('#barChartLoaderTotal').dimmer('hide');

    })

}

function drawPieChart(json, year, destinationDiv, piquetName) {

    Highcharts.chart(destinationDiv, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'RTU - Related (' + piquetName + ') (' + year + ')'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.0f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: json

    });

}

function drawBarChart(json, year, destinationDiv, piquetName) {

    Highcharts.chart(destinationDiv, {
        chart: {
            type: 'column'
        },
        title: {
            text: piquetName + ' RTU Related by month (' + year + ')'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of interventions'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: json

    });

}