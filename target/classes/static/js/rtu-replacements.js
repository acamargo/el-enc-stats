/**
 * Created by alejandrocamargo on 20/3/17.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    //initialize semantic-ui components
    initializeTopMenu()

    getTheData()


}

function getTheData() {

    $('#barChartLoader').dimmer('show');
    $('#stackedBarChartLoader').dimmer('show');

    $.get('/api/rtuReplacements/barChart', function(response) {
        var json = response

        drawBarChart(json, 'barChart')
        $('#barChartLoader').dimmer('hide');

    })

    $.get('/api/rtuReplacements/stackedBarChart',
        function(response) {

            var json = response

            var currentYear  =new Date().getFullYear();

            $.get('/api/year/2014/' + currentYear, function(response) {
                var yearCategories = response
                drawStackedBarChart(json, 'stackedBarChart', yearCategories)
                $('#stackedBarChartLoader').dimmer('hide');
            })

    })



}

function drawBarChart(json, destinationDiv) {

    Highcharts.chart(destinationDiv, {
        chart: {
            type: 'column'
        },
        title: {
            text: 'RTU replacements per year'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        xAxis: {
            categories: ['Year'],
            crosshair: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total replacements'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: json

    });
}

function drawStackedBarChart(json, destinationDiv, yearCategories) {

    Highcharts.chart(destinationDiv, {
        chart: {
            type: 'column'
        },
        title: {
            text: 'RTU replacements per type'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        xAxis: {
            categories: yearCategories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total replacements'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },

        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: json
    });

}

