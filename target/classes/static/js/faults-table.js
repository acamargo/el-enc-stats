/**
 * Created by alejandrocamargo on 24/3/17.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2009/' + (currentYear), function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });

    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });

    var table = $('#table').DataTable( {
        "autoWidth": false,
        "oLanguage": {
            "sLengthMenu": "Entries: _MENU_ </br>",
            "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries"

        },

    });

    var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
            'csvHtml5',
            'excelHtml5',
            'pdfHtml5',
            'print'
        ]
    }).container().appendTo($('#buttons'));

}

function yearSelected() {

    $('#tableLoader').dimmer('show');

    var year = $('#year').val();

    $.get('/api/faults/' + year, function (response) {

        var json = response

        var dataSet = [];

        json.forEach(function(e) {

            var newArray = [];
            newArray.push(moment(e.created).format("YYYY-MM-DD h:mm:ss"));
            newArray.push(e.origin_unit);
            newArray.push(e.subject);
            newArray.push(e.content);
            dataSet.push(newArray);


        })

        $('#table').dataTable().fnClearTable();
        $('#table').dataTable().fnAddData(dataSet);

        $('#tableLoader').dimmer('hide');

    })

}
