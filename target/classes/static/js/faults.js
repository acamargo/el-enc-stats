/**
 * Created by alejandrocamargo on 10/3/17.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

    loadData()

});

function initializeComponents() {

    initializeTopMenu()

}

function loadData() {

    $('#majorFaultsLineChartLoader').dimmer('show');
    $('#minorFaultsLineChartLoader').dimmer('show');

    $.get('/api/majorFaults/EN-EL/lineChart/', function(response) {
        var json = response

        drawLineChart(json, "EN-EL Major Faults per year", "majorFaultsLineChart", "Major Faults", 2009)
        $('#majorFaultsLineChartLoader').dimmer('hide');

    })

    $.get('/api/minorFaults/EN-EL/lineChart/', function(response) {
        var json = response

        drawLineChart(json, "EN-EL Minor Faults per year", "minorFaultsLineChart", "Minor Faults", 2009)
        $('#minorFaultsLineChartLoader').dimmer('hide');

    })

}

function drawLineChart(json, title, contentName, yAxisTitle, yearStart) {

    Highcharts.chart(contentName, {
        title: {
            text: title,
            x: -20 //center
        },
        subtitle: {
            text: 'Source: OP-TI LogBook',
            x: -20
        },

        plotOptions: {
            series: {
                pointStart: yearStart
            }
        },

        yAxis: {
            title: {
                text: yAxisTitle
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },

        series: json

    });

}