package ch.cern.statistics.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.Assert.*;

/**
 * Created by alejandrocamargo on 15/3/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DateUtilTest {

    @Test
    public void testGetDayOfTheWeekFromDate() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            //Tuesday
            Date date = sdf.parse("15-03-2016");

            System.out.println("TUESDAY: " + DateUtil.getDayOfTheWeekFromDate(date));

            assertTrue(1 == DateUtil.getDayOfTheWeekFromDate(date));

            //Sunday
            date = sdf.parse("20-03-2016");
            assertTrue(6 == DateUtil.getDayOfTheWeekFromDate(date));


            //Monday
            date = sdf.parse("14-03-2016");
            assertTrue(0 == DateUtil.getDayOfTheWeekFromDate(date));


        } catch (Exception e) {

            throw new RuntimeException();

        }


    }

    @Test
    public void testGetHourOfTheDayFromDate() {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        try {

            Date date = sdf.parse("22:23:52");

            assertTrue(22 == DateUtil.getHourOfTheDayFromDate(date));


        } catch (Exception e) {

            throw new RuntimeException();

        }


    }
}
