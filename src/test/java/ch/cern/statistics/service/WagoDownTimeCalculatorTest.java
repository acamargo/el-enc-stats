package ch.cern.statistics.service;

import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.entities.lhclogging.TagValue;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;
import ch.cern.statistics.service.lhclogging.WagoDownTimeCalculator;
import ch.cern.statistics.service.lhclogging.impl.LhcLoggingImportImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.transform.sax.SAXSource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alejandrocamargo on 18/5/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WagoDownTimeCalculatorTest {

    @Autowired
    WagoDownTimeCalculator downTimeCalculator;

    @Autowired
    LhcLoggingImport lhcLoggingImport;

    @Test
    public void testImport() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;

        try {

            date = sdf.parse("12-05-2017");

            assertEquals(2, downTimeCalculator.importTags(date, false).size());

        } catch (Exception e) {


        }

    }

    @Test
    public void test(){

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;

        try {

            date = sdf.parse("15-09-2016");

            List<WagoDownTime> ls = downTimeCalculator.importTags(date, false);

            assertEquals(1, ls.size());

        } catch (Exception e) {


        }

    }

    @Test
    public void test2(){

        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            date = sdf.parse("18-01-2017");
        } catch (Exception e) {

        }

        List<TagValue> ls = lhcLoggingImport.importData(date, "ETC701/5E_OK");

        List<WagoDownTime> lsW = downTimeCalculator.calculateDownTimes(ls);

        assertEquals(4, lsW.size());
    }


    @Test
    public void bulkTest() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {

            Date dateStart = null;

            Date dateEnd = null;

            dateStart = sdf.parse("01-05-2017");

            dateEnd = new Date();

            List<WagoDownTime> ls = downTimeCalculator.bulkImport(dateStart, dateEnd, false);

            System.out.println();

        } catch (Exception e) {


        }


    }

}
