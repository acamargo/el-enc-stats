package ch.cern.statistics.service;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.service.logbook.LogBookRestClient;
import ch.cern.statistics.service.logbook.impl.LogBookRestClientImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * Created by alejandrocamargo on 21/5/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogBookRestClientTest {

    @Autowired
    LogBookRestClient logBookRestClient;

    @Test
    public void getMajorfaultsTest() {

        logBookRestClient.getMajorFaults(Fault.OriginUnitEnum.EL).forEach(e -> System.out.println(e.toString()));

    }


}
