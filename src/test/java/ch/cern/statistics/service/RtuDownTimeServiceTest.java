package ch.cern.statistics.service;

import ch.cern.statistics.entities.Rtu;
import ch.cern.statistics.entities.RtuDownTime;
import ch.cern.statistics.repositories.RtuRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RtuDownTimeServiceTest {

    @Autowired
    private RtuDownTimeService rtuDownTimeService;

    @Autowired
    private RtuRepository rtuRepository;

    List<String> rtuList;

    @Before
    public void init() {

        String[] rtus = {"ETC01*84",
                "ETC01/A5",
                "ETC01/HM0",
                "ETC03*11",
                "ETC03*23",
                "ETC03*25",
                "ETC03*4",
                "ETC03*43",
                "ETC03*59",
                "ETC03*6",
                "ETC03*62",
                "ETC03*9",
                "ETC03/12",
                "ETC03/15",
                "ETC03/15A",
                "ETC03/18",
                "ETC03/1DX",
                "ETC03/1E",
                "ETC03/22",
                "ETC03/25",
                "ETC03/28",
                "ETC03/2E",
                "ETC03/32",
                "ETC03/33",
                "ETC03/38",
                "ETC03/3E",
                "ETC03/3Z",
                "ETC03/42",
                "ETC03/45",
                "ETC03/48",
                "ETC03/4E",
                "ETC03/52",
                "ETC03/55",
                "ETC03/56",
                "ETC03/58",
                "ETC03/5E",
                "ETC03/62",
                "ETC03/65",
                "ETC03/68",
                "ETC03/6E",
                "ETC03/72",
                "ETC03/76",
                "ETC03/78",
                "ETC03/7E",
                "ETC03/82",
                "ETC03/85",
                "ETC03/85X",
                "ETC03/88",
                "ETC03/8E",
                "ETC03/9E",
                "ETC03/A1",
                "ETC03/A2",
                "ETC03/A3",
                "ETC03/A4",
                "ETC03/A5",
                "ETC03/A6",
                "ETC03/A7",
                "ETC03/A80",
                "ETC03/A81",
                "ETC03/A82",
                "ETC03/A85",
                "ETC03/BE",
                "ETC03/E18",
                "ETC03/E9",
                "ETC03/E91",
                "ETC04*43",
                "ETC04*9",
                "ETC04/15A",
                "ETC04/55",
                "ETC04/9E",
                "ETC04/BE",
                "ETC04/E91"};

        rtuList = Arrays.asList(rtus);

    }

//    @Test
//    public void insertRtus() {
//
//        rtuList.forEach(e -> rtuRepository.save(new Rtu(e)));
//
//    }

    @Test
    public void getResults() {

        try {
            Thread.sleep(3000);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            Date start = sdf.parse("2016-01-01 00:00:00.000");
            Date end = sdf.parse("2016-12-31 23:59:59.999");

            rtuList.forEach(r -> {

                List<RtuDownTime> ls = rtuDownTimeService.findDownTimesByRtuBetweenDates(r, start, end);

                ls.forEach(e -> {

                    System.out.println(e.toCsvDateFormatted(new SimpleDateFormat("MMMM")));

                });

            });




        } catch (Exception e) {

        }

    }

}
