package ch.cern.statistics.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EamServiceTest {

    @Autowired
    private EamService eamService;

    @Autowired
    private Environment env;


    @Test
    public void testOpPiquet() {

        eamService.getOpPiquetsByYear(2016).forEach((k, v) -> System.out.println(k + " ----> " + v));

    }

    @Test
    public void testCoPiquet() {

        eamService.getCoPiquetsByYear(2016).forEach((k, v) -> System.out.println(k + " ----> " + v));

    }

    @Test
    public void testRtuByArea() {

        eamService.getInterventionsByRtuByYear(2015, "ECCP").forEach(e -> {
            System.out.println(e.getEvtObject() + " " + e.getTimes());

        });

    }

}
