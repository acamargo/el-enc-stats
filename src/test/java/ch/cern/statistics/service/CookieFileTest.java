package ch.cern.statistics.service;

import ch.cern.statistics.service.logbook.CookieReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;

/**
 * Created by alejandrocamargo on 23/5/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class CookieFileTest {

    @Autowired
    private CookieReader cookieReader;

    @Test
    public void readCookieFileTest() {

        ReflectionTestUtils.setField(cookieReader, "cookieFile", "src/test/resources/cookie.txt");

        String str = cookieReader.readCookieFile();

        System.out.println(str);

        assertEquals(str.split("=")[0], "FedAuth");

    }

}
