package ch.cern.statistics.service;

import ch.cern.statistics.repositories.FaultRepository;
import ch.cern.statistics.repositories.MajorFaultRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by alejandrocamargo on 2/3/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogbookServiceTest {

    @Autowired
    LogbookService service;

    @Autowired
    MajorFaultRepository majorFaultRepository;

    @Autowired
    FaultRepository faultRepository;


    @Test
    public void testGetFaults() {

//        service.getFaults("EN-EL").forEach(f -> faultRepository.save(f));
//
//        service.getFaults("EN-EL-ENC").forEach(f -> faultRepository.save(f));
//
//        service.getFaults("EN-EL-OP").forEach(f -> faultRepository.save(f));

    }

    @Test
    public void testGetMajorFaults() {

//        service.getMajorFaults("EN-EL").forEach(f -> majorFaultRepository.save(f));
//
//        service.getMajorFaults("EN-EL-ENC").forEach(f -> majorFaultRepository.save(f));
//
//        service.getMajorFaults("EN-EL-OP").forEach(f -> majorFaultRepository.save(f));

    }
}
