package ch.cern.statistics.service;

import ch.cern.statistics.entities.Year;
import ch.cern.statistics.repositories.YearRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.Assert.*;

/**
 * Created by alejandrocamargo on 19/3/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class YearRepositoryTest {

    @Autowired
    private YearRepository repository;

    @Test
    public void testRepository() {

        assertEquals(repository.findYearsBetweenDates(2010, 2016).size(), 7);

    }
}
