package ch.cern.statistics.service;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.repositories.FaultRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by alejandrocamargo on 23/3/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class FaultRepositoryTest {

    @Autowired
    FaultRepository faultRepository;

    @Test
    public void testRepository() {

        List<Fault> ls = faultRepository.getByOriginUnit(AbstractFault.OriginUnitEnum.EL.toString());

        ls.forEach(e -> System.out.println(e.getSubject()));

    }
}
