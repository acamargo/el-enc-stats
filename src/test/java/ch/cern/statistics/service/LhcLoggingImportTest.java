package ch.cern.statistics.service;

import ch.cern.statistics.entities.lhclogging.TagValue;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by alejandrocamargo on 17/5/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LhcLoggingImportTest {

    @Autowired
    LhcLoggingImport lhcLoggingImport;

    @Test
    public void testService() {

        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            date = sdf.parse("12-05-2017");
        } catch (Exception e) {

        }

        List<TagValue> ls = lhcLoggingImport.importData(date, "ETC701/E91_OK");

        assertEquals(8, ls.size());

        ls.forEach(e -> System.out.println(e.getDate() + " --> " + e.getValue()));

    }

    @Test
    public void testService2() {

        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            date = sdf.parse("18-01-2017");
        } catch (Exception e) {

        }

        List<TagValue> ls = lhcLoggingImport.importData(date, "ETC701/5E_OK");

        ls.forEach(e -> System.out.println(e.getDate() + " --> " + e.getValue()));

    }
}
