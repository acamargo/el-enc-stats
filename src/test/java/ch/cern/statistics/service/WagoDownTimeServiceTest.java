package ch.cern.statistics.service;

import ch.cern.statistics.entities.Wago;
import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.repositories.WagoDownTimeRepository;
import ch.cern.statistics.repositories.WagoDownTimeRepositoryCustom;
import ch.cern.statistics.repositories.WagoRepository;
import ch.cern.statistics.util.DateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class WagoDownTimeServiceTest {

    @Autowired
    WagoRepository wagoRepository;

    @Autowired
    WagoDownTimeService service;

    @Autowired
    WagoDownTimeRepository repository;


    @Test
    public void test() {

        String year = "2016";

        List<WagoDownTime> ls = service.findDownTimesByWagoBetweenDates("ETC701/2E", DateUtil.getDate("01-01-" + year + " 00:00:00"), DateUtil.getDate("31-12-" + year + " 23:59:59"));

        List<WagoDownTime> ls2 = repository.findWagoBetweenDates("ETC701/2E", DateUtil.getDate("01-01-" + year + " 00:00:00"), DateUtil.getDate("31-12-" + year + " 23:59:59"));

        System.out.print("");

    }


}
