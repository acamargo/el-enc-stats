package ch.cern.statistics.service;

import ch.cern.statistics.service.charts.RtuDownTimeChartService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by alejandrocamargo on 14/3/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class RtuDownTimeChartServiceTest {

    @Autowired
    RtuDownTimeChartService service;

    @Test
    public void test(){
        System.out.println(service.getYearlyDownTimePerRtuPieChart("ETC03/2E", 2016));
    }

    @Test
    public void heatMapTest() {

        //service.getRtuHeatMap("ETC03/A1", 2016);

        System.out.println(service.getRtuHeatMapCount("ETC03/A1", 2016));

    }


}
