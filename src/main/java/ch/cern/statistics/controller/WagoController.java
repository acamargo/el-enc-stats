package ch.cern.statistics.controller;

import ch.cern.statistics.entities.json.DropDown;
import ch.cern.statistics.entities.json.DropDownResult;
import ch.cern.statistics.repositories.WagoDownTimeRepository;
import ch.cern.statistics.repositories.WagoRepository;
import ch.cern.statistics.service.WagoDownTimeService;
import ch.cern.statistics.util.Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 7/4/17.
 */

@RestController
public class WagoController {

    public WagoRepository repository;

    public WagoController(WagoRepository repository) {
        this.repository = repository;
    }


    @RequestMapping("/api/wagos/dropDown/")
    public DropDown getWagosDropDown() {

        //DropDown dd = Utils.getDropdownFromStringList(repository.getWagoNames());

        DropDown dd = Utils.getDropdownFromStringList(repository.findAll()
                .stream()
                .map(e -> e.getName())
                .collect(Collectors.toList()));

        //dd.getResults().add(new DropDownResult("All WAGOs", "ALL_WAGOS"));

        return dd;

    }

}
