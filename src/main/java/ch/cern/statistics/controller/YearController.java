package ch.cern.statistics.controller;

import ch.cern.statistics.service.YearService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alejandrocamargo on 20/3/17.
 */

@RestController
public class YearController {

    private YearService yearService;

    public YearController(YearService yearService) {
        this.yearService = yearService;
    }

    @RequestMapping(value = "/api/year/{start}/{end}", method = RequestMethod.GET)
    public List<String> getYearStringSeries(@PathVariable(name = "start") Integer start,
                                            @PathVariable(name = "end") Integer end) {

        return yearService.getYearListBetweenDates(start, end);

    }

}
