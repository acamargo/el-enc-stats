package ch.cern.statistics.controller;

import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.service.charts.CoPiquetChartService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by acamargo on 07/02/2017.
 */

@RestController
public class CoPiquetController {

    private CoPiquetChartService chartService;

    public CoPiquetController(CoPiquetChartService chartService) {
        this.chartService = chartService;
    }

    @RequestMapping("/api/coPiquet/ECCO/barChart/{year}")
    public List<Serie<Integer>> getEccoBarChartSeries(@PathVariable(name = "year") Integer year) {

        return chartService.getRtuByYearBarChartSeries(year, CoPiquetEnum.ECCO.toString());

    }

    @RequestMapping("/api/coPiquet/ECCP/barChart/{year}")
    public List<Serie<Integer>> getEccpBarChartSeries(@PathVariable(name = "year") Integer year) {

        return chartService.getRtuByYearBarChartSeries(year, CoPiquetEnum.ECCP.toString());

    }

    @RequestMapping("/api/coPiquet/RTU/total/pie/{year}")
    public List<PieChartSerie> getTotalRtuPieChart(@PathVariable(name = "year") Integer year) {

        return chartService.getTotalCoPiquetRtuRelatedSeries(year);

    }

    @RequestMapping("/api/coPiquet/RTU/ECCP/pie/{year}")
    public List<PieChartSerie> getEccpRtupieChart(@PathVariable(name = "year") Integer year) {

        return chartService.getCoPiquetRturelatedSeriesByCode(year, CoPiquetEnum.ECCP);

    }

    @RequestMapping("/api/coPiquet/RTU/ECCO/pie/{year}")
    public List<PieChartSerie> getEccoRtupieChart(@PathVariable(name = "year") Integer year) {

        return chartService.getCoPiquetRturelatedSeriesByCode(year, CoPiquetEnum.ECCO);

    }

    @RequestMapping("/api/coPiquet/RTU/total/bar/{year}")
    public List<Serie> getTotalRtuBarChart(@PathVariable(name = "year") Integer year) {

        return chartService.getPiquetSeriesBarChart(year);

    }

}
