package ch.cern.statistics.controller;

import ch.cern.statistics.entities.Rtu;
import ch.cern.statistics.entities.json.DropDown;
import ch.cern.statistics.repositories.RtuRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class RtuController {

    private RtuRepository rtuRepository;

    //Autowired
    public RtuController(RtuRepository rtuRepository) {
        this.rtuRepository = rtuRepository;
    }


    @RequestMapping("/api/rtus")
    public List<Rtu> getRtus() {

        return rtuRepository.findAll();

    }

    @RequestMapping("/api/rtus/dropDown/")
    public DropDown getRtusDropDown() {

        List<Rtu> rtuList = rtuRepository.findAll();

        rtuList.add(new Rtu("All RTUs", "ALL_RTU"));

        Collections.sort(rtuList);

        return Rtu.getRtusDropdown(rtuList);



    }


}
