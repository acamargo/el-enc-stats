package ch.cern.statistics.controller;

import ch.cern.statistics.entities.HeatMapSerie;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.service.charts.RtuDownTimeChartService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RtuDownTimeController {

    private RtuDownTimeChartService chartService;


    public RtuDownTimeController(RtuDownTimeChartService chartService) {
        this.chartService = chartService;
    }

    @RequestMapping(value = "/api/rtuDownTime/{year}", method = RequestMethod.POST)
    public List<Serie> getSeries(@RequestParam(value = "rtus[]") List<String> rtuNames,
                                 @PathVariable(name = "year") Integer year) {

        List<Serie> ls = chartService.getNumberRtuDownTimesPerYear(rtuNames, year);

        return ls;

    }

    @RequestMapping(value = "/api/rtuDownTime/pieChart/{hostName}/{year}", method = RequestMethod.GET)
    public List<PieChartSerie> getPieChartSerie(@PathVariable(name = "hostName") String hostName,
                                                @PathVariable(name = "year") Integer year) {

        return chartService.getYearlyDownTimePerRtuPieChart(hostName, year);

    }

    @RequestMapping(value = "/api/rtuDownTime/heatMap/{hostName}/{year}", method = RequestMethod.GET)
    public List<long[]> getHeatMapSerie(@PathVariable(name = "hostName") String hostName,
                                        @PathVariable(name = "year") Integer year) {

        if ("ALL_RTU".equals(hostName)) {

            return chartService.getRtuHeatMapCountAllRtus(year).jsonPrint();

        } else {

            return chartService.getRtuHeatMapCount(hostName, year).jsonPrint();
        }

    }




}
