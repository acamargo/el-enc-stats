package ch.cern.statistics.controller;

import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;
import ch.cern.statistics.service.charts.FaultChartService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 10/3/17.
 */

@RestController
public class FaultController {

    private FaultChartService faultChartService;

    public FaultController(FaultChartService faultChartService) {
        this.faultChartService = faultChartService;
    }

    @RequestMapping("/api/majorFaults/EN-EL/lineChart/")
    public List<Serie> getLineChart() {

        return faultChartService.getMajorFaultsAnualLineChart(2009, Calendar.getInstance().get(Calendar.YEAR));

    }


    @RequestMapping("/api/minorFaults/EN-EL/lineChart/")
    public List<Serie> getLineChartMinor() {

        return faultChartService.getMinorFaultsAnualLineChart(2009, Calendar.getInstance().get(Calendar.YEAR));

    }


    @RequestMapping("/api/majorFaults/EN-EL/barChart/{year}")
    public List<Serie> getBarChartMajor(@PathVariable(name = "year") Integer year) {

        return faultChartService.getMajorFaultsBarChart(year);

    }


    @RequestMapping("/api/minorFaults/EN-EL/barChart/{year}")
    public List<Serie> getBarChartMinor(@PathVariable(name = "year") Integer year) {

        return faultChartService.getMinorFaultsBarChart(year);

    }

    @RequestMapping("/api/majorFaults/{year}")
    public List<MajorFault> getMajorFaults(@PathVariable(name = "year") Integer year) {

        return faultChartService.getMajorFaultsDataTable(year);

    }

    @RequestMapping("/api/faults/{year}")
    public List<Fault> getFaults(@PathVariable(name = "year") Integer year) {

        return faultChartService.getFaultsDataTable(year);

    }




}
