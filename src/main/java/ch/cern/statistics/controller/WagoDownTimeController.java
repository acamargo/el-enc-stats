package ch.cern.statistics.controller;

import ch.cern.statistics.entities.DownTimeStatistics;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.service.WagoDownTimeService;
import ch.cern.statistics.service.charts.WagoDownTimeChartService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */

@RestController
public class WagoDownTimeController {

    private WagoDownTimeChartService chartService;

    public WagoDownTimeController(WagoDownTimeChartService chartService) {
        this.chartService = chartService;
    }

    @RequestMapping(value = "/api/wagoDownTime/pieChart/{deviceName}/{year}", method = RequestMethod.GET)
    public List<PieChartSerie> getPieChartSerie(@PathVariable(name = "deviceName") String deviceName,
                                                @PathVariable(name = "year") Integer year) {

        return chartService.getYearlyDownTimePerWagoPieChart(deviceName, year);

    }

    @RequestMapping(value = "/api/wagoDownTime/statistics/{deviceName}/{year}", method = RequestMethod.GET)
    public DownTimeStatistics getStatistics(@PathVariable(name = "deviceName") String deviceName,
                                            @PathVariable(name = "year") Integer year) {

        return chartService.getWagoDownTimeStatistics(deviceName, year);

    }

}
