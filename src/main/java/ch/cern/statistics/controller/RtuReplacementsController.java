package ch.cern.statistics.controller;

import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.service.charts.RtuReplacementsChartService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alejandrocamargo on 19/3/17.
 */

@RestController
public class RtuReplacementsController {

    private RtuReplacementsChartService service;

    //autowired constructor
    public RtuReplacementsController(RtuReplacementsChartService service) {
        this.service = service;
    }


    @RequestMapping(value = "/api/rtuReplacements/barChart", method = RequestMethod.GET)
    public List<Serie> getBarChartSeries() {

        return service.getReplacementsByYearSeries();

    }

    @RequestMapping(value = "/api/rtuReplacements/stackedBarChart", method = RequestMethod.GET)
    public List<Serie> getStackedBarChartSeries() {

        return service.getReplacementsByYearByRtuTypeSeries();

    }
}
