package ch.cern.statistics.controller;

import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.service.charts.PiquetOverviewChartService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PiquetOverviewController {

    private PiquetOverviewChartService piquetOverviewChartService;

    public PiquetOverviewController(PiquetOverviewChartService piquetOverviewChartService) {
        this.piquetOverviewChartService = piquetOverviewChartService;
    }

    @RequestMapping("/api/piquetOverview/barChart/{year}")
    public List<Serie> getBarChartSeries(@PathVariable(name = "year") Integer year) {

        return piquetOverviewChartService.getPiquetSeriesBarChart(year);

    }

    @RequestMapping("/api/piquetOverview/pieChart/{year}")
    public List<PieChartSerie> getPieChartSeries(@PathVariable(name = "year") Integer year) {

        return piquetOverviewChartService.getPiquetSeriesPieChart(year);

    }

    @RequestMapping("/api/piquetOverview/lineChart/{year}")
    public List<Serie> getLineChartSeries(@PathVariable(name = "year") Integer year) {

        return piquetOverviewChartService.getPiquetSeriesLineChart(year);

    }
}
