package ch.cern.statistics.configuration;


import ch.cern.statistics.controller.*;
import ch.cern.statistics.repositories.*;
import ch.cern.statistics.repositories.impl.EamJdbcRepositoryImpl;
import ch.cern.statistics.repositories.impl.WagoDownTimeRepositoryImpl;
import ch.cern.statistics.service.*;
import ch.cern.statistics.service.charts.*;
import ch.cern.statistics.service.charts.impl.*;
import ch.cern.statistics.service.impl.*;
import ch.cern.statistics.service.lhclogging.ImportScheduler;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;
import ch.cern.statistics.service.lhclogging.WagoDownTimeCalculator;
import ch.cern.statistics.service.lhclogging.impl.ImportSchedulerImpl;
import ch.cern.statistics.service.lhclogging.impl.LhcLoggingImportImpl;
import ch.cern.statistics.service.lhclogging.impl.WagoDownTimeCalculatorImpl;
import ch.cern.statistics.service.linux.LinuxCommandService;
import ch.cern.statistics.service.linux.impl.LinuxCommandServiceImpl;
import ch.cern.statistics.service.logbook.CookieGenerator;
import ch.cern.statistics.service.logbook.CookieReader;
import ch.cern.statistics.service.logbook.LogBookRestClient;
import ch.cern.statistics.service.logbook.impl.CookieGeneratorImpl;
import ch.cern.statistics.service.logbook.impl.CookieReaderImpl;
import ch.cern.statistics.service.logbook.impl.LogBookRestClientImpl;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;


@Configuration
@EnableScheduling
@EnableMongoRepositories(basePackages = "ch.cern.statistics.repositories")
public class Beans {

    @Autowired
    private Environment env;

    /** Repositories **/

    @Autowired
    private RtuDownTimeRepository rtuDownTimeRepository;

    @Autowired
    private WagoDownTimeRepository wagoDownTimeRepository;

    @Autowired
    private RtuRepository rtuRepository;

    @Autowired
    private WagoRepository wagoRepository;

    @Autowired
    private YearRepository yearRepository;

    @Autowired
    private FaultRepository faultRepository;

    @Autowired
    private MajorFaultRepository majorFaultRepository;

    @Bean
    public WagoDownTimeRepositoryCustom wagoDownTimeRepositoryCustom(MongoTemplate mongoTemplate) {
        return new WagoDownTimeRepositoryImpl(mongoTemplate);
    }

    @Bean
    public EamJdbcRepository eamJdbcRepository(JdbcTemplate jdbcTemplate) {
        return new EamJdbcRepositoryImpl(jdbcTemplate);
    }

    /** Services **/

    @Bean
    public RtuDownTimeChartService chartService(RtuDownTimeService rtuDownTimeService) {
        return new RtuDownTimeChartServiceImpl(rtuDownTimeService);
    }

    @Bean
    public RtuDownTimeService rtuDownTimeService() {
        return new RtuDownTimeServiceImpl(rtuDownTimeRepository);
    }

    @Bean
    public WagoDownTimeService wagoDownTimeService() {
        return new WagoDownTimeServiceImpl(wagoDownTimeRepository);
    }

    @Bean
    public WagoDownTimeChartService wagoDownTimeChartService(WagoDownTimeService wagoDownTimeService) {
        return new WagoDownTimeChartServiceImpl(wagoDownTimeService, wagoDownTimeRepository);
    }

    @Bean
    public EamService eamService(EamJdbcRepository eamJdbcRepository) {
        return new EamServiceImpl(eamJdbcRepository);
    }

    @Bean
    public PiquetOverviewChartService piquetOverviewChartService(EamService eamService) {
        return new PiquetOverviewChartServiceImpl(eamService);
    }

    @Bean
    public CoPiquetChartService coPiquetChartService(EamService eamService) {
        return new CoPiquetChartServiceImpl(eamService);
    }

    @Bean
    public LogbookService logbookServiceMongoDb(MajorFaultRepository majorFaultRepository,
                                                FaultRepository faultRepository) {
        return new LogbookServiceMongoDbImpl(majorFaultRepository, faultRepository);
    }

    @Bean
    public LogbookService logbookServiceRest(LogBookRestClient logBookRestClient) {
        return new LogbookServiceRestImpl(logBookRestClient);
    }

    @Bean
    public FaultChartService faultChartService(@Qualifier("logbookServiceRest") LogbookService logbookService) {
        return new FaultChartServiceImpl(logbookService);
    }

    @Bean
    public RtuReplacementsChartService rtuReplacementsChartService(EamService eamService,
                                                                   YearRepository yearRepository) {
        return new RtuReplacementsChartServiceImpl(eamService, yearRepository);
    }

    @Bean
    public YearService yearService(YearRepository repository) {
        return new YearServiceImpl(repository);
    }

    @Bean
    public LhcLoggingImport lhcLoggingImport(RestTemplate restTemplate) {
        return new LhcLoggingImportImpl(restTemplate);
    }

    @Bean
    public WagoDownTimeCalculator downTimeCalculator(WagoRepository wagoRepository,
                                                     WagoDownTimeRepository wagoDownTimeRepository,
                                                     LhcLoggingImport lhcLoggingImport) {
        return new WagoDownTimeCalculatorImpl(wagoRepository, wagoDownTimeRepository, lhcLoggingImport);
    }

    @Bean
    public ImportScheduler importScheduler(WagoDownTimeCalculator wagoDownTimeCalculator) {
        return new ImportSchedulerImpl(wagoDownTimeCalculator);
    }

    @Bean
    public LogBookRestClient logBookRestClient(RestTemplate restTemplate, CookieReader cookieReader) {
        return new LogBookRestClientImpl(restTemplate, cookieReader);
    }

    @Bean
    public LinuxCommandService linuxCommandService() {
        return new LinuxCommandServiceImpl();
    }

    @Bean
    public CookieGenerator cookieGenerator(LinuxCommandService linuxCommandService) {
        return new CookieGeneratorImpl(linuxCommandService);
    }

    @Bean
    public CookieReader cookieReader() {
        return new CookieReaderImpl();
    }

    /** Controllers **/

    @Bean
    public RtuDownTimeController rtuDownTimeController(RtuDownTimeChartService chartService) {
        return new RtuDownTimeController(chartService);
    }

    @Bean
    public PiquetOverviewController piquetOverviewController(PiquetOverviewChartService piquetOverviewChartService) {
        return new PiquetOverviewController(piquetOverviewChartService);
    }

    @Bean
    public CoPiquetController coPiquetController(CoPiquetChartService piquetChartService) {
        return new CoPiquetController(piquetChartService);
    }

    @Bean
    public RtuController rtuController(RtuRepository rtuRepository) {
        return new RtuController(rtuRepository);
    }

    @Bean
    public FaultController faultController(FaultChartService faultChartService) {
        return new FaultController(faultChartService);
    }

    @Bean
    public RtuReplacementsController rtuReplacementsController(
            RtuReplacementsChartService rtuReplacementsChartService) {
        return new RtuReplacementsController(rtuReplacementsChartService);
    }

    @Bean
    public YearController yearController(YearService yearService) {
        return new YearController(yearService);
    }

    @Bean
    public WagoDownTimeController wagoDownTimeController(WagoDownTimeChartService chartService) {
        return new WagoDownTimeController(chartService);
    }

    @Bean
    public WagoController wagoController(WagoRepository wagoRepository) {
        return new WagoController(wagoRepository);
    }

    /** Data Sources **/

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {

        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DataSource dataSource() {

        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName(env.getProperty("ensdm.driver"));
        ds.setUrl(env.getProperty("ensdm.url"));
        ds.setUsername(env.getProperty("ensdm.username"));
        ds.setPassword(env.getProperty("ensdm.password"));
        ds.setInitialSize(5);
        ds.setMaxActive(10);

        return ds;

    }

    /** Other **/

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }


}
