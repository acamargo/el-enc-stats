package ch.cern.statistics.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by acamargo on 07/02/2017.
 */
public class EamRtuInterventionsPerYear {

    @Getter
    @Setter
    public String evtObject;

    @Getter
    @Setter
    public Integer times;

    public EamRtuInterventionsPerYear() {
    }

    public EamRtuInterventionsPerYear(String evtObject, Integer times) {
        this.evtObject = evtObject;
        this.times = times;
    }

    public String getEvtObject() {
        return evtObject;
    }

    public void setEvtObject(String evtObject) {
        this.evtObject = evtObject;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }
}
