package ch.cern.statistics;

import ch.cern.statistics.service.logbook.CookieGenerator;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableCaching
@EnableEncryptableProperties
@EnableScheduling
public class StatisticsApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext context = SpringApplication.run(StatisticsApplication.class, args);

		/** Authenticate to Kerberos & generate the Cookie **/

		CookieGenerator cookieGenerator = context.getBean(CookieGenerator.class);

		cookieGenerator.kerberosAuthentication();

	}
}
