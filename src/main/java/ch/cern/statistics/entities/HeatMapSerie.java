package ch.cern.statistics.entities;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandrocamargo on 16/3/17.
 */
public class HeatMapSerie {

    private List<HeatMapEntry> entries = new ArrayList<>();

    public void pushEntry(HeatMapEntry entry) {

        entries.add(entry);

    }

    public List<HeatMapEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<HeatMapEntry> entries) {
        this.entries = entries;
    }

    public List<long[]> jsonPrint() {

        List<long[]> ls = new ArrayList<>();

        entries.forEach(e -> ls.add(e.toArray()));

        return ls;

    }
}
