package ch.cern.statistics.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by alejandrocamargo on 24/5/17.
 */

@Document(collection = "DAU")
public class Dau implements Comparable<Dau> {

    @Id
    @Getter
    @Setter
    private String name;

    public Dau(String name) {

        this.name = name;

    }

    private String getNameScaped() {
        return name.replace("/", "_");
    }

    private String getNameUnScaped() {
        return name.replace("_", "/");
    }

    @Override
    public int compareTo(Dau o) {

        return this.getName().compareTo(o.getName());
    }
}
