package ch.cern.statistics.entities;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Serie<T> {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private List<T> data;

    public Serie() {

        data = new ArrayList<>();
    }

    public void addData(T value) {
        data.add(value);
    }
}
