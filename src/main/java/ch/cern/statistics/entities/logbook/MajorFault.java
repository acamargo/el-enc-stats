package ch.cern.statistics.entities.logbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by alejandrocamargo on 22/3/17.
 */

@Document(collection = "MAJOR_FAULT")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MajorFault extends AbstractFault {

    @Id
    @Getter
    @Setter
    @JsonProperty("major_fault_id")
    private Long majorFaultId;

    @Getter
    @Setter
    @JsonProperty("edms_id")
    private Long edmsId;

    @Getter
    @Setter
    @JsonProperty("edms_error")
    private String edmsError;

    @Getter
    @Setter
    @JsonProperty("tioc_conclusion")
    private String tiocConclusion;

    @Getter
    @Setter
    private Boolean approved;

    @Getter
    @Setter
    @JsonProperty("event_type_name")
    private String eventTypeName;

    @Getter
    @Setter
    @JsonProperty("AD")
    private String ad;

    @Getter
    @Setter
    @JsonProperty("ALICE")
    private String alice;

    @Getter
    @Setter
    @JsonProperty("ATLAS")
    private String atlas;

    @Getter
    @Setter
    @JsonProperty("Accelerators")
    private String accelerators;

    @Getter
    @Setter
    @JsonProperty("Administrative sector")
    private String administrativeSector;

    @Getter
    @Setter
    @JsonProperty("Booster")
    private String booster;

    @Getter
    @Setter
    @JsonProperty("CAST")
    private String cast;

    @Getter
    @Setter
    @JsonProperty("CERN")
    private String cern;

    @Getter
    @Setter
    @JsonProperty("CMS")
    private String cms;

    @Getter
    @Setter
    @JsonProperty("CNGS")
    private String cngs;
    
    @Getter
    @Setter
    @JsonProperty("CRYOGENICS")
    private String cryogenic;

    @Getter
    @Setter
    @JsonProperty("Computer centre")
    private String computerCentre;
    
    @JsonProperty("East Hall")
    @Getter
    @Setter
    private String eastHall;
    
    @JsonProperty("ISOLDE")
    @Getter
    @Setter
    private String isolde;

    @JsonProperty("LEIR")
    @Getter
    @Setter
    private String leir;
    
    @JsonProperty("LHC")
    @Getter
    @Setter
    private String lhc;
    
    @Getter
    @Setter
    @JsonProperty("LHCb")
    private String lhcb;

    @Getter
    @Setter
    @JsonProperty("LINAC")
    private String linac;

    @Getter
    @Setter
    @JsonProperty("Linac2")
    private String linac2;

    @Getter
    @Setter
    @JsonProperty("Linac3")
    private String linac3;

    @Getter
    @Setter
    @JsonProperty("North Area")
    private String northArea;

    @Getter
    @Setter
    @JsonProperty("PS")
    private String ps;

    @Getter
    @Setter
    @JsonProperty("SPS")
    private String sps;

    @Getter
    @Setter
    @JsonProperty("nTof")
    private String nTof;

}
