package ch.cern.statistics.entities.logbook;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by alejandrocamargo on 22/3/17.
 */
public abstract class AbstractFault {


    public enum OriginUnitEnum {

        EL {

            public String toString() {
                return "EN-EL";
            }

        },

        OP {

            public String toString() {
                return "EN-EL-OP";
            }

        },

        ENC {

            public String toString() {
                return "EN-EL-ENC";
            }

        }
    }

    @Getter
    @Setter
    private Date created;

    @Getter
    @Setter
    private Date modified;

    @Getter
    @Setter
    private Integer status;

    @Getter
    @Setter
    private String creator;

    @Getter
    @Setter
    private String modifier;

    @Getter
    @Setter
    @JsonProperty("input_source_id")
    private Integer inputSourceId;

    @Getter
    @Setter
    @JsonProperty("parent_id")
    private Integer parentId;

    @Getter
    @Setter
    @JsonProperty("start_date")
    private Date startDate;

    @Getter
    @Setter
    @JsonProperty("end_date")
    private Date endDate;

    @Getter
    @Setter
    private String subject;

    @Getter
    @Setter
    @JsonProperty("event_type")
    private Integer eventType;

    @Getter
    @Setter
    @JsonProperty("origin_equipment")
    private String originEquipment;

    @Getter
    @Setter
    @JsonProperty("origin_fault_category")
    private String originFaultCategory;

    @Getter
    @Setter
    @JsonProperty("origin_unit")
    private String originUnit;

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private String location;

    @Getter
    @Setter
    @JsonProperty("ti_informed_date")
    private Date tiInformedDate;
}
