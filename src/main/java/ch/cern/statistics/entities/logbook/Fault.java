package ch.cern.statistics.entities.logbook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by alejandrocamargo on 2/3/17.
 */

@Document(collection = "FAULT")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fault extends AbstractFault {

    @Id
    @Getter
    @Setter
    @JsonProperty("fault_id")
    private Long faultId;

    @Getter
    @Setter
    @JsonProperty("event_type_name")
    private String eventTypeName;

}
