package ch.cern.statistics.entities.json;

import lombok.Getter;
import lombok.Setter;

public class DropDownResult {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String value;


    public DropDownResult(String name, String value) {

        this.name = name;
        this.value = value;


    }
}
