package ch.cern.statistics.entities.json;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class DropDown {

    @Getter
    @Setter
    private boolean success;

    @Getter
    @Setter
    private List<DropDownResult> results;

    public DropDown() {

        results = new ArrayList<>();

        success = true;
    }
}
