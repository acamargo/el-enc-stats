package ch.cern.statistics.entities;


import lombok.Getter;
import lombok.Setter;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public class DownTimeStatistics {

    @Getter
    @Setter
    private long totalDownTimes = 0;

    @Getter
    @Setter
    private long totalDownTimeInSeconds = 0;

    @Getter
    @Setter
    private double averageDownTime = 0;


    public void calculateAverageDownTime() {

        if (totalDownTimes != 0) {

            averageDownTime = (double) (totalDownTimeInSeconds / totalDownTimes);

        } else {

            averageDownTime = 0;

        }

    }

}
