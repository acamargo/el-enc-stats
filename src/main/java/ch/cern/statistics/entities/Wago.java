package ch.cern.statistics.entities;

import ch.cern.statistics.entities.json.DropDown;
import ch.cern.statistics.entities.json.DropDownResult;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by alejandrocamargo on 17/5/17.
 */

@Document(collection = "WAGO")
public class Wago implements Comparable<Wago> {

    @Id
    @Getter
    @Setter
    private String name;

    public Wago(String name) {

        this.name = name;

    }

    private String getNameScaped() {
        return name.replace("/", "_");
    }

    private String getNameUnScaped() {
        return name.replace("_", "/");
    }

    public static DropDown getWagoDropdown(List<Wago> wagoList) {

        DropDown dd = new DropDown();

        wagoList.stream().forEach(r -> dd.getResults().add(new DropDownResult(r.getNameUnScaped(), r.getNameScaped())));

        return dd;

    }

    @Override
    public int compareTo(Wago o) {

        return this.getName().compareTo(o.getName());
    }


}
