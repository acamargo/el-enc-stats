package ch.cern.statistics.entities.lhclogging;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by alejandrocamargo on 13/4/17.
 */
public class TagValue implements Comparable<TagValue> {

    private String tagName;

    private Timestamp timestamp;

    private double value;

    @JsonIgnore
    private Date date;

    public TagValue() {
    }

    public TagValue(String tagName, Timestamp timestamp, double value) {
        this.tagName = tagName;
        this.timestamp = timestamp;
        this.value = value;
    }

    @Override
    public int compareTo(TagValue o) {

        if (timestamp.after(o.getTimestamp())) return 1;

        else if (timestamp.before(o.getTimestamp())) return -1;

        else return 0;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {

        this.timestamp = timestamp;

        this.date = new Date(timestamp.getTime());

    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }
}
