package ch.cern.statistics.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

/**
 * Created by alejandrocamargo on 19/3/17.
 */

@Document(collection = "YEAR")
public class Year {

    @Id
    private String yearString;

    private Integer year;

    public Year() {

    }

    public Year(String yearString, Integer year) {
        this.yearString = yearString;
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getYearString() {
        return yearString;
    }

    public void setYearString(String yearString) {
        this.yearString = yearString;
    }
}
