package ch.cern.statistics.entities;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by alejandrocamargo on 6/4/17.
 */

@Document(collection = "WAGO_DOWN_TIME")
public class WagoDownTime extends DownTime {

    @Override
    public String toString() {
        return "WagoDownTime { "
                + "startTime=" + startTime
                + ", endTime=" + endTime
                + ", wagoName=" + deviceName
                + ", downTime=" + downTimeInSeconds() + " s }";
    }
}
