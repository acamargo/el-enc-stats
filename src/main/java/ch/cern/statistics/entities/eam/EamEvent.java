package ch.cern.statistics.entities.eam;


import lombok.Getter;
import lombok.Setter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class EamEvent {

    @Getter
    @Setter
    private String evtDesc;

    @Getter
    @Setter
    private String evtMrc;

    @Getter
    @Setter
    private Date evtDate;

    @Getter
    @Setter
    private String evtObject;

    public EamEvent() {

    }

    public EamEvent(String evtDesc, String evtMrc, Date evtDate, String evtObject) {
        this.evtDesc = evtDesc;
        this.evtMrc = evtMrc;
        this.evtDate = evtDate;
        this.evtObject = evtObject;
    }

    public static EamEvent mapEamEvent(ResultSet rs) throws SQLException {

        return new EamEvent(rs.getString("EVT_DESC"),
                rs.getString("EVT_MRC"), rs.getDate("EVT_DATE"), rs.getString("EVT_OBJECT"));

    }

}
