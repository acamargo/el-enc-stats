package ch.cern.statistics.entities;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
@Document(collection = "DAU_DOWN_TIME")
public class DauDownTime extends DownTime {

    @Override
    public String toString() {
        return "DauDownTime {"
                + "id='" + id + '\''
                + ", startTime=" + startTime
                + ", endTime=" + endTime
                + ", deviceName='" + deviceName + '\''
                + '}';
    }
}
