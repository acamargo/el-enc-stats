package ch.cern.statistics.entities;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by alejandrocamargo on 16/3/17.
 */
public class HeatMapEntry {

    @Getter
    @Setter
    private Long day;

    @Getter
    @Setter
    private Long hour;

    @Getter
    @Setter
    private Long value;

    public HeatMapEntry(Long day, Long hour, Long value) {

        this.day = day;
        this.hour = hour;
        this.value = value;
    }

    public long[] toArray() {

        long [] array = new long[3];

        array[0] = hour;
        array[1] = day;
        array[2] = value;

        return array;

    }
}
