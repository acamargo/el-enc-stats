package ch.cern.statistics.entities;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "RTU_DOWN_TIME")
public class RtuDownTime extends DownTime {

    @Override
    public String toString() {
        return "RtuDownTime { "
                + "startTime=" + startTime
                + ", endTime=" + endTime
                + ", rtuName=" + deviceName
                + ", downTime=" + downTimeInSeconds() + " s }";
    }

}
