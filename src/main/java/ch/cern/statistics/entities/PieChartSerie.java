package ch.cern.statistics.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acamargo on 06/02/2017.
 */
public class PieChartSerie {

    private String name;

    private List<PieChartData> data = new ArrayList<>();

    public void addNewSerie(String name, Double value) {

        data.add(new PieChartData(name, value));

    }

    public class PieChartData {

        private String name;

        private Double y;

        public PieChartData(String name, Double y) {
            this.name = name;
            this.y = y;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getY() {
            return y;
        }

        public void setY(Double y) {
            this.y = y;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PieChartData> getData() {
        return data;
    }

    public void setData(List<PieChartData> data) {
        this.data = data;
    }
}
