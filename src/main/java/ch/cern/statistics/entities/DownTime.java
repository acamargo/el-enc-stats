package ch.cern.statistics.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

/**
 * Created by alejandrocamargo on 6/4/17.
 */
public abstract class DownTime {

    @Id
    @Getter
    @Setter
    protected String id;

    @Getter
    @Setter
    protected Date startTime;

    @Getter
    @Setter
    protected Date endTime;

    @Getter
    @Setter
    protected String deviceName;

    public Long downTimeInSeconds() {

        return (endTime.getTime() - startTime.getTime()) / 1000;

    }

    @Override
    public String toString() {
        return "DownTime { "
                + "startTime=" + startTime
                + ", endTime=" + endTime
                + ", deviceName=" + deviceName
                + ", downTime=" + downTimeInSeconds() + " s }";
    }

    public String toCsv() {

        StringJoiner sj = new StringJoiner(";");

        sj.add(deviceName);
        sj.add(startTime.toString());
        sj.add(endTime.toString());
        sj.add(downTimeInSeconds().toString());

        return sj.toString();

    }

    public String toCsvDateFormatted(SimpleDateFormat sdf) {

        StringJoiner sj = new StringJoiner(";");

        sj.add(deviceName);

        sj.add(sdf.format(startTime));
        sj.add(sdf.format(endTime));

        sj.add(downTimeInSeconds().toString());

        return sj.toString();

    }
}
