package ch.cern.statistics.entities;

import ch.cern.statistics.entities.json.DropDown;
import ch.cern.statistics.entities.json.DropDownResult;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "RTU")
public class Rtu implements Comparable<Rtu> {


    @Id
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String hostName;

    public Rtu() {

    }

    public Rtu(String name) {

        this.name = name;

    }

    public Rtu(String name, String hostName) {

        this.name = name;
        this.hostName = hostName;

    }

    public static DropDown getRtusDropdown(List<Rtu> rtuList) {

        DropDown dd = new DropDown();

        rtuList.stream().forEach(r -> dd.getResults().add(new DropDownResult(r.getNameUnScaped(), r.getNameScaped())));

        return dd;

    }

    private String getNameScaped() {
        return name.replace("/", "_");
    }

    private String getNameUnScaped() {
        return name.replace("_", "/");
    }

    @Override
    public int compareTo(Rtu o) {

        return this.getName().compareTo(o.getName());
    }
}
