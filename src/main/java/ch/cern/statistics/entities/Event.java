package ch.cern.statistics.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class Event implements Comparable<Event> {

    @Getter
    @Setter
    private Date time;

    @Getter
    @Setter
    private String device;

    @Getter
    @Setter
    private String state;

    @Getter
    @Setter
    private String action;

    public boolean isAlarmCame() {

        if ("Alarm CAME".equalsIgnoreCase(action)) return true;
        else return false;

    }

    @Override
    public int compareTo(Event e) {

        if (e.getTime().after(this.getTime())) return -1;
        else if (e.getTime().before(this.getTime())) return 1;
        else return 0;

    }

    @Override
    public String toString() {
        return "Event { "
                + "time=" + time
                + ", device='" + device + '\''
                + ", state='" + state + '\''
                + ", action='" + action + '\''
                + '}';
    }
}
