package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 23/3/17.
 */
public interface FaultRepository extends MongoRepository<Fault, Long> {

    @Query(value = "{'originUnit': ?0}")
    List<Fault> getByOriginUnit(String originUnit);

    @Query(value = "{'created': { $gte: ?0 , $lt:  ?1 } }")
    List<Fault> getBetweenDates(Date start, Date end);

}
