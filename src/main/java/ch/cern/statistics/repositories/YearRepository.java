package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.Year;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by alejandrocamargo on 19/3/17.
 */
public interface YearRepository  extends MongoRepository<Year, String> {


    @Query(value = "{'year': { $gte: ?0 , $lte:  ?1 } }")
    List<Year> findYearsBetweenDates(Integer startYear, Integer endYear);


}
