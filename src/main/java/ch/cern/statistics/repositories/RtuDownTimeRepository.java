package ch.cern.statistics.repositories;


import ch.cern.statistics.entities.RtuDownTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

public interface RtuDownTimeRepository extends MongoRepository<RtuDownTime, String> {

    List<RtuDownTime> findByDeviceName(String name);

    @Query(value = "{'deviceName': ?0, 'startTime': { $gte: ?1 , $lt:  ?2 } }")
    List<RtuDownTime> findRtuBetweenDates(String deviceName, Date dateStart, Date dateEnd);

    @Query(value = "{'startTime': { $gte: ?0 , $lt:  ?1 } }")
    List<RtuDownTime> findBetweenDates(Date dateStart, Date dateEnd);

}
