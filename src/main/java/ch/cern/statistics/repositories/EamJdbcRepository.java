package ch.cern.statistics.repositories;

import ch.cern.statistics.dto.EamRtuInterventionsPerYear;
import ch.cern.statistics.dto.Pair;
import ch.cern.statistics.entities.eam.EamEvent;

import java.util.List;

/**
 * Created by alejandrocamargo on 5/2/17.
 */
public interface EamJdbcRepository {

    Integer queryNumberPiquetByYear(Integer year, String piquetCode);

    List<EamEvent> getPiquetsByYearByCode(Integer year, String piquetCode);

    List<EamEvent> getRtuRelatedInterventionsByYear(Integer year, String piquetCode);

    List<EamRtuInterventionsPerYear> getRtuRelatedInterventionsByRtuByYear(Integer year, String piquetCode);

    List<EamEvent> getRtuReplacementsByYear(Integer year);

    List<EamEvent> getAllRtuReplacements();

    void pingDatabase();


}
