package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.Dau;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by alejandrocamargo on 24/5/17.
 */
public interface DauRepository extends MongoRepository<Dau, String> {
}
