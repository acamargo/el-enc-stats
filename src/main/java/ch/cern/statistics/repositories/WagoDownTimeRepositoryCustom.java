package ch.cern.statistics.repositories;

import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public interface WagoDownTimeRepositoryCustom {

    List<String> getWagoNames();
}
