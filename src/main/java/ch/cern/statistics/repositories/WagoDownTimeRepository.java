package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.WagoDownTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public interface WagoDownTimeRepository extends MongoRepository<WagoDownTime, String>, WagoDownTimeRepositoryCustom  {

    List<WagoDownTime> findByDeviceName(String deviceName);

    @Query(value = "{'deviceName': ?0, 'startTime': { $gte: ?1 , $lt:  ?2 } }")
    List<WagoDownTime> findWagoBetweenDates(String deviceName, Date dateStart, Date dateEnd);

    @Query(value = "{'startTime': { $gte: ?0 , $lt:  ?1 } }")
    List<WagoDownTime> findBetweenDates(Date dateStart, Date dateEnd);

}
