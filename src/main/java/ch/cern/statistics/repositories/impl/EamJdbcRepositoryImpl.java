package ch.cern.statistics.repositories.impl;

import ch.cern.statistics.dto.EamRtuInterventionsPerYear;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.repositories.EamJdbcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

public class EamJdbcRepositoryImpl implements EamJdbcRepository {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JdbcOperations jdbcOperations;

    public EamJdbcRepositoryImpl(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Cacheable("numberPiquetsByYearCache")
    public Integer queryNumberPiquetByYear(Integer year, String piquetCode) {

        String[] toDates = prepareToDateForFullYear(year);

        String sql =
                "SELECT count(*) FROM R5EVENTS@EAM.WORLD \n"
                        + "WHERE EVT_MRC= ? \n"
                        + "AND EVT_DATE >= " + toDates[0] + "\n"
                        + "AND EVT_DATE <= " + toDates[1] + "\n"
                        + "AND EVT_TYPE = 'JOB'\n"
                        + "ORDER BY EVT_DATE DESC";

        return jdbcOperations.queryForObject(sql, new Object[] { piquetCode }, Integer.class);

    }

    @Cacheable("piquetsByYearByCodeCache")
    public List<EamEvent> getPiquetsByYearByCode(Integer year, String piquetCode) {

        String[] toDates = prepareToDateForFullYear(year);

        String sql =
                "SELECT EVT_DESC, EVT_MRC, EVT_DATE, EVT_OBJECT FROM R5EVENTS@EAM.WORLD \n"
                        + "WHERE EVT_MRC= ? \n"
                        + "AND EVT_DATE >= " + toDates[0] + "\n"
                        + "AND EVT_DATE <= " + toDates[1] + "\n"
                        + "AND EVT_TYPE = 'JOB'\n"
                        + "ORDER BY EVT_DATE DESC";


        return jdbcOperations.query(sql, new Object[] { piquetCode }, new BeanPropertyRowMapper(EamEvent.class));

    }

    @Cacheable("rtuRelatedByYearCache")
    public List<EamEvent> getRtuRelatedInterventionsByYear(Integer year, String piquetCode) {

        String[] toDates = prepareToDateForFullYear(year);

        String sql =
                "SELECT EVT_DESC, EVT_MRC, EVT_DATE, EVT_OBJECT FROM R5EVENTS@EAM.WORLD \n"
                        + "WHERE EVT_MRC= ? \n"
                        + "AND EVT_DATE >= " + toDates[0] + "\n"
                        + "AND EVT_DATE <= " + toDates[1] + "\n"
                        + "AND \n"
                        + "(\n"
                        + "UPPER(EVT_DESC) like '% RTU %'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC01%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC03%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC04%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC05%'\n"
                        + ")\n";

        return jdbcOperations.query(sql, new Object[] { piquetCode }, new BeanPropertyRowMapper(EamEvent.class));
    }

    @Cacheable("rtuRelatedByRtuByYearCache")
    public List<EamRtuInterventionsPerYear> getRtuRelatedInterventionsByRtuByYear(Integer year, String piquetCode) {

        String[] toDates = prepareToDateForFullYear(year);

        String sql =
                "SELECT evt_object, count(*) as TIMES FROM R5EVENTS@EAM.WORLD \n"
                        + "WHERE EVT_MRC= ? \n"
                        + "AND EVT_DATE >= " + toDates[0] + "\n"
                        + "AND EVT_DATE <= " + toDates[1] + "\n"
                        + "AND \n"
                        + "(\n"
                        + "UPPER(EVT_DESC) like '% RTU %'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC01%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC03%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC04%'\n"
                        + "OR\n"
                        + "UPPER(EVT_OBJECT) like '%ETC05%'\n"
                        + ")\n"
                        + "GROUP BY evt_object\n";

        return jdbcOperations.query(sql, new Object[] { piquetCode },
                new BeanPropertyRowMapper(EamRtuInterventionsPerYear.class));
    }

    public List<EamEvent> getRtuReplacementsByYear(Integer year) {

        String[] toDates = prepareToDateForFullYear(year);

        String sql =
                "SELECT * FROM R5EVENTS@EAM.WORLD\n"
                        + "WHERE EVT_MRC = 'ECCO'\n"
                        + "AND EVT_DESC LIKE '%<-- MAGASIN%'\n"
                        + "AND EVT_DATE >= " + toDates[0] + "\n"
                        + "AND EVT_DATE <= " + toDates[1] + "\n";

        return jdbcOperations.query(sql, new BeanPropertyRowMapper(EamEvent.class));


    }

    public List<EamEvent> getAllRtuReplacements() {

        String sql =
                "SELECT * FROM R5EVENTS@EAM.WORLD\n"
                        + "WHERE EVT_MRC = 'ECCO'\n"
                        + "AND EVT_DESC LIKE '%<-- MAGASIN%'\n";

        return jdbcOperations.query(sql, new BeanPropertyRowMapper(EamEvent.class));


    }


    private String[] prepareToDateForFullYear(Integer year) {

        String[] toDates = new String[2];

        toDates[0] = "to_date('01-01-" + year.toString() + " 00:00:00', 'dd-MM-yyyy HH24:MI:SS')";
        toDates[1] = "to_date('31-12-" + year.toString() + " 23:59:59', 'dd-MM-yyyy HH24:MI:SS')";

        return toDates;

    }

    @Scheduled(cron = "0 * * * * *")
    public void pingDatabase() {

        String sql = "SELECT * FROM R5EVENTS@EAM.WORLD";

        jdbcOperations.execute(sql);

    }
}
