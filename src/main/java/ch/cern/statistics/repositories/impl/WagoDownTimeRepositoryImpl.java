package ch.cern.statistics.repositories.impl;

import ch.cern.statistics.repositories.WagoDownTimeRepositoryCustom;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public class WagoDownTimeRepositoryImpl implements WagoDownTimeRepositoryCustom {

    private MongoTemplate mongoTemplate;

    public WagoDownTimeRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<String> getWagoNames() {

        return mongoTemplate.getCollection("WAGO_DOWN_TIME").distinct("deviceName");

    }
}
