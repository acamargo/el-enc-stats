package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.Wago;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by alejandrocamargo on 17/5/17.
 */
public interface WagoRepository extends MongoRepository<Wago, String> {
}
