package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.DauDownTime;
import ch.cern.statistics.entities.WagoDownTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
public interface DauDownTimeRepository extends MongoRepository<DauDownTime, String> {

        List<DauDownTime> findByDeviceName(String deviceName);

        @Query(value = "{'deviceName': ?0, 'startTime': { $gte: ?1 , $lt:  ?2 } }")
        List<DauDownTime> findWagoBetweenDates(String deviceName, Date dateStart, Date dateEnd);

        @Query(value = "{'startTime': { $gte: ?0 , $lt:  ?1 } }")
        List<DauDownTime> findBetweenDates(Date dateStart, Date dateEnd);

}
