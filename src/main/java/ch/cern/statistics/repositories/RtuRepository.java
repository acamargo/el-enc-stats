package ch.cern.statistics.repositories;

import ch.cern.statistics.entities.Rtu;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface RtuRepository extends MongoRepository<Rtu, String> {
}
