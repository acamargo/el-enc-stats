package ch.cern.statistics.util;

import ch.cern.statistics.entities.Rtu;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.OpPiquetEnum;
import ch.cern.statistics.entities.json.DropDown;
import ch.cern.statistics.entities.json.DropDownResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Utils {

    public static List<String> getPiquets() {

        List<String> ls =  new ArrayList<>();

        Arrays.asList(CoPiquetEnum.values()).forEach(e -> ls.add(e.toString()));
        Arrays.asList(OpPiquetEnum.values()).forEach(e -> ls.add(e.toString()));

        return ls;

    }

    public static List<OpPiquetEnum> getOpPiquets() {

        List<OpPiquetEnum> ls =  new ArrayList<>();

        Arrays.asList(OpPiquetEnum.values()).forEach(e -> ls.add(e));

        return ls;
    }

    public static List<CoPiquetEnum> getCoPiquets() {

        List<CoPiquetEnum> ls =  new ArrayList<>();

        Arrays.asList(CoPiquetEnum.values()).forEach(e -> ls.add(e));

        return ls;
    }

    public static String scapeDeviceName(String deviceName) {
        return deviceName.replace("/", "_");
    }

    public static String unscapeDeviceName(String deviceName) {
        return deviceName.replace("_", "/");
    }

    public static DropDown getDropdownFromStringList(List<String> deviceNames) {

        DropDown dd = new DropDown();

        Collections.sort(deviceNames);

        deviceNames.stream().forEach(r -> dd.getResults()
                .add(new DropDownResult(unscapeDeviceName(r), scapeDeviceName(r))));

        return dd;

    }

}
