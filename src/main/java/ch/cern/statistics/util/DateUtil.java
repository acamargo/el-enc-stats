package ch.cern.statistics.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {

    public static final long SECONDS_IN_YEAR = 31536000;


    public static Date getDate(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Date ret;

        try {

            synchronized (sdf) {

                ret = sdf.parse(date);
            }

        } catch (Exception p) {

            throw new RuntimeException("Could not parse date: " + date);

        }

        return ret;

    }

    public static Integer getDayOfTheWeekFromDate(Date date) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(date);

        int day = cal.get(Calendar.DAY_OF_WEEK);

        if (day == 1) day = 6;
        else day = day - 2;

        return day;

    }

    public static Integer getHourOfTheDayFromDate(Date date) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(date);

        return cal.get(Calendar.HOUR_OF_DAY);


    }

    public static Integer getCurrentYear() {

        Calendar cal = Calendar.getInstance();

        return cal.get(Calendar.YEAR);

    }

}
