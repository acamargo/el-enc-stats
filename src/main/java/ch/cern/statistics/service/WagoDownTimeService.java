package ch.cern.statistics.service;

import ch.cern.statistics.entities.WagoDownTime;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public interface WagoDownTimeService {

    List<WagoDownTime> findDownTimeByWago(String wagoName);

    List<WagoDownTime> findDownTimesByWagoBetweenDates(String wagoName, Date dateStart, Date dateEnd);

    List<WagoDownTime> findDownTimesBetweenDates(Date dateStart, Date dateEnd);

}
