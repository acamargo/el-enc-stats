package ch.cern.statistics.service;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;

import java.util.List;

/**
 * Created by alejandrocamargo on 2/3/17.
 */
public interface LogbookService {

    List<Fault> getFaults(AbstractFault.OriginUnitEnum  originUnit);

    List<MajorFault> getMajorFaults(AbstractFault.OriginUnitEnum  originUnit);

    List<Fault> getFaultsByYear(Integer year);

    List<MajorFault> getMajorFaultsByYear(Integer year);

}
