package ch.cern.statistics.service;

import java.util.List;

/**
 * Created by alejandrocamargo on 20/3/17.
 */
public interface YearService {

     List<String> getYearListBetweenDates(Integer startYear, Integer endYear);

}
