package ch.cern.statistics.service;


import ch.cern.statistics.entities.Year;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.dto.EamRtuInterventionsPerYear;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.MonthEnum;
import ch.cern.statistics.entities.enums.OpPiquetEnum;

import java.util.List;
import java.util.Map;

public interface EamService {

    Map<OpPiquetEnum, Integer> getOpPiquetsByYear(Integer year);

    Map<CoPiquetEnum, Integer> getCoPiquetsByYear(Integer year);

    Map<MonthEnum, List<EamEvent>> getPiquetsByMonth(Integer year, String piquetCode);

    List<EamRtuInterventionsPerYear> getInterventionsByRtuByYear(Integer year, String piquetCode);

    Map<MonthEnum, List<EamEvent>> getRtuPiquetsByMonth(Integer year, String piquetCode);

    List<EamEvent> getRtuReplacementsByYear(String year);

    Map<Integer, Integer> getCountAllRtuReplacementsByYear(List<Year> yearList);

    Map<Integer, List<EamEvent>> getAllRtuReplacementsByYear(List<Year> yearList);
}
