package ch.cern.statistics.service;


import ch.cern.statistics.entities.Event;

import java.io.File;
import java.util.List;

public interface CsvService {
    
    List<Event> parseCsv(File csvFile);

}
