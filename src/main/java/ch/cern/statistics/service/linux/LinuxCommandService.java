package ch.cern.statistics.service.linux;

/**
 * Created by alejandrocamargo on 23/5/17.
 */
public interface LinuxCommandService {

    int executeCommand(String[] command, String comment, boolean hasArguments);

}
