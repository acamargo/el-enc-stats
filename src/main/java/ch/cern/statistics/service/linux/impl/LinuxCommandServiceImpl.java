package ch.cern.statistics.service.linux.impl;

import ch.cern.statistics.service.linux.LinuxCommandService;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Created by alejandrocamargo on 23/5/17.
 */
public class LinuxCommandServiceImpl implements LinuxCommandService {

    private final Logger logger = Logger.getLogger(LinuxCommandServiceImpl.class);

    /**
     * Executes the given command in the local Linux system. In case of a piped command,
     * the array of Strings should be like:
     *
     *      String[] cmd = {
     *                      "/bin/sh",
     *                      "-c",
     *                      "ls /etc | grep release"
     *                      };
     *
     * In case of a non-piped command, the command is expected in the position 0 of the String[]
     * In case of multiple arguments, pass them in different positions of the array
     *
     * @param command to send to the linux shell
     * @param comment for logging purposes
     * @param hasArguments true if you are using a pipe or passing arguments in the command
     *
     * @return -1 NOK, 0 if OK
     */
    public int executeCommand(String[] command, String comment, boolean hasArguments) {

        try {

            Process p;

            if (!hasArguments) {

                logger.info("Executing command: " + comment);

                p = Runtime.getRuntime().exec(command[0]);

            } else {

                logger.info("Executing piped command: " + comment);

                p = Runtime.getRuntime().exec(command);

            }


            p.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(),
                    StandardCharsets.UTF_8));

            String line;

            while ((line = reader.readLine()) != null) {

                logger.info(line);

            }

        } catch (IOException e1) {

            logger.error(e1.getMessage());

            return -1;

        } catch (InterruptedException e2) {

            logger.error(e2.getMessage());

            return -1;

        }

        return 0;

    }

}
