package ch.cern.statistics.service;

import ch.cern.statistics.entities.RtuDownTime;

import java.util.Date;
import java.util.List;

public interface RtuDownTimeService {

    List<RtuDownTime> findDownTimeByRtu(String rtuName);

    List<RtuDownTime> findDownTimesByRtuBetweenDates(String rtuName, Date dateStart, Date dateEnd);

    List<RtuDownTime> findDownTimesBetweenDates(Date dateStart, Date dateEnd);


}
