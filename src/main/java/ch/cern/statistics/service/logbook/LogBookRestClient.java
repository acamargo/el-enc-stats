package ch.cern.statistics.service.logbook;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;

import java.util.List;

/**
 * Created by alejandrocamargo on 21/5/17.
 */
public interface LogBookRestClient {

    List<MajorFault> getMajorFaults(AbstractFault.OriginUnitEnum originUnit);

    List<Fault> getMinorFaults(AbstractFault.OriginUnitEnum originUnit);

}
