package ch.cern.statistics.service.logbook;

/**
 * Created by alejandrocamargo on 23/5/17.
 */
public interface CookieReader {

    String readCookieFile();

}
