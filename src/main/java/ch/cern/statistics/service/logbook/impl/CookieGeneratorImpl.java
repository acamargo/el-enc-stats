package ch.cern.statistics.service.logbook.impl;

import ch.cern.statistics.service.linux.LinuxCommandService;
import ch.cern.statistics.service.logbook.CookieGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import org.apache.log4j.Logger;


/**
 * Created by alejandrocamargo on 22/5/17.
 */
public class CookieGeneratorImpl implements CookieGenerator {

    private final Logger logger = Logger.getLogger(CookieGeneratorImpl.class);

    private LinuxCommandService linuxCommandService;

    @Value("${sso.auth.username}")
    private String username;

    @Value("${sso.auth.password}")
    private String password;

    @Value("${sso.cookiefile.location}")
    private String cookieLocation;

    private final String urlToAuthenticate =
            "https://op-webtools.web.cern.ch/logbook/backend/api/v1/faults/?limit=25&tag=!hidden&origin_unit=EN-EL";


    public CookieGeneratorImpl(LinuxCommandService linuxCommandService) {
        this.linuxCommandService = linuxCommandService;
    }

    /**
     * Authenticates the application in Kerberos
     * it runs when the application starts, then every 6 hours
     * Warning: it is platform-dependent (Linux)
     */
    @Scheduled(initialDelay = 21600000, fixedRate = 21600000)
    public void kerberosAuthentication() {

        deleteCookie();

        logger.info("Authenticating to Kerberos with username: " + username);

        String [] command2 = {"/bin/sh", "-c", "echo " + password + " | kinit ", username};

        linuxCommandService.executeCommand(command2, "Kerberos authentication (kinit)", true);

        generateCookie();

    }

    /**
     * Renews the kerberos authentication
     * then it generates a new Cookie file
     *
     * Warning: it is platform-dependent (Linux)
     */

    public void renewKerberosAuthentication() {

        String [] command = {"kinit -R"};

        linuxCommandService.executeCommand(command, "Kerberos renewal (kinit -R)", false);

        generateCookie();

    }

    /**
     * Generates the Cookie file by using the command cern-get-sso-cookie
     *
     * Warning: it is platform-dependent (Linux)
     * @url http://linux.web.cern.ch/linux/docs/cernssocookie.shtml
     *
     */
    public void generateCookie() {

        String [] command = {"cern-get-sso-cookie", "--krb", "-r", "-u", urlToAuthenticate, "-o", cookieLocation};

        linuxCommandService.executeCommand(command, "Generating SSO Cookie", true);

    }

    /**
     * Deletes the cookie file
     */
    public void deleteCookie() {

        String [] command = {"rm", cookieLocation};

        linuxCommandService.executeCommand(command, "Deleting old cookie file", true);

    }

}
