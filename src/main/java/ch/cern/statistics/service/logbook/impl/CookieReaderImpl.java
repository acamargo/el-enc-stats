package ch.cern.statistics.service.logbook.impl;

import ch.cern.statistics.service.logbook.CookieReader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by alejandrocamargo on 23/5/17.
 */
public class CookieReaderImpl implements CookieReader {

    private final Logger logger = Logger.getLogger(CookieReaderImpl.class);

    @Value("${sso.cookiefile.location}")
    private String cookieFile;

    private Function<String, String> splitFunction = (s ->  {

            try {

                String[] array = s.split("\\s+");

                return array[5] + "=" + array[6];

            } catch (Exception e) {

                logger.error("Problem parsing line: " + s);

                return "";
            }

        }

    );

    public String readCookieFile() {

        List<String> cookies;

        try (Stream<String> stream = Files.lines(Paths.get(cookieFile))) {

            cookies = stream
                    .filter(l -> l.contains("FedAuth"))
                    .map(splitFunction)
                    .collect(Collectors.toList());

        } catch (IOException e) {

            logger.error("SSO Authentication impossible - Cannot read cookie file! " + cookieFile);

            throw new RuntimeException("SSO Authentication impossible - Cannot read cookie file! ");

        }

        if (cookies.size() > 0) return cookies.get(0);

        else {

            logger.error("SSO Authentication impossible - Cookie file does not contain any FedAuth entry! "
                    + cookieFile);

        }

        return "";

    }

}
