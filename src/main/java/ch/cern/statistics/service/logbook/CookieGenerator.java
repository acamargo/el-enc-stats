package ch.cern.statistics.service.logbook;

/**
 * Created by alejandrocamargo on 22/5/17.
 */
public interface CookieGenerator {

    void kerberosAuthentication();

    void renewKerberosAuthentication();

    void generateCookie();

    void deleteCookie();

}
