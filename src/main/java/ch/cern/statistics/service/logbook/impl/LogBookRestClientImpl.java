package ch.cern.statistics.service.logbook.impl;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;
import ch.cern.statistics.service.logbook.CookieReader;
import ch.cern.statistics.service.logbook.LogBookRestClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;


/**
 * Created by alejandrocamargo on 21/5/17.
 */
public class LogBookRestClientImpl implements LogBookRestClient {

    private final Logger logger = Logger.getLogger(LogBookRestClientImpl.class);

    @Value("${logbook.majorfaults.url}")
    private String majorFaultsUrl;

    @Value("${logbook.faults.url}")
    private String faultsUrl;

    private RestTemplate restTemplate;

    private CookieReader cookieReader;

    public LogBookRestClientImpl(RestTemplate restTemplate, CookieReader cookieReader) {
        this.restTemplate = restTemplate;
        this.cookieReader = cookieReader;
    }

    public List<MajorFault> getMajorFaults(AbstractFault.OriginUnitEnum originUnit) {

        logger.info("Sending REST request: Major Faults " + originUnit.toString());

        String authCookieText = cookieReader.readCookieFile();

        //logger.info(authCookieText);

        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.add("Cookie", authCookieText);

        HttpEntity request = new HttpEntity(null, requestHeaders);

        ResponseEntity<List<MajorFault>> response = restTemplate
                .exchange(majorFaultsUrl + originUnit.toString(), HttpMethod.GET, request,
                        new ParameterizedTypeReference<List<MajorFault>>(){});

        return response.getBody();

    }

    public List<Fault> getMinorFaults(AbstractFault.OriginUnitEnum originUnit) {

        logger.info("Sending REST request: Faults " + originUnit.toString());

        String authCookieText = cookieReader.readCookieFile();

        //logger.info(authCookieText);

        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.add("Cookie", authCookieText);

        HttpEntity request = new HttpEntity(null, requestHeaders);

        ResponseEntity<List<Fault>> response = restTemplate
                .exchange(faultsUrl + originUnit.toString(), HttpMethod.GET, request,
                        new ParameterizedTypeReference<List<Fault>>(){});

        return response.getBody();

    }

}
