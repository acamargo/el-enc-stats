package ch.cern.statistics.service.lhclogging;

import ch.cern.statistics.entities.DauDownTime;
import ch.cern.statistics.service.lhclogging.GenericDownTimeCalculator;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
public interface DauDownTimeCalculator extends GenericDownTimeCalculator<DauDownTime> {

    List<DauDownTime> importTags(Date day, boolean persist);

    List<DauDownTime> bulkImport(Date startDate, Date endDate, boolean persist);

    List<DauDownTime> importYesterdaysTags(boolean persist);
}
