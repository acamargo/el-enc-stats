package ch.cern.statistics.service.lhclogging;

import ch.cern.statistics.entities.DownTime;
import ch.cern.statistics.entities.lhclogging.TagValue;

import java.util.List;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
public interface GenericDownTimeCalculator<T extends DownTime> {

    List<T> calculateDownTimes(List<TagValue> tagValues);

}
