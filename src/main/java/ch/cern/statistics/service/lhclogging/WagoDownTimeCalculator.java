package ch.cern.statistics.service.lhclogging;

import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.entities.lhclogging.TagValue;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 18/5/17.
 */
public interface WagoDownTimeCalculator extends GenericDownTimeCalculator<WagoDownTime> {

    List<WagoDownTime> importTags(Date day, boolean persist);

    List<WagoDownTime> bulkImport(Date startDate, Date endDate, boolean persist);

    List<WagoDownTime> importYesterdaysTags(boolean persist);
}
