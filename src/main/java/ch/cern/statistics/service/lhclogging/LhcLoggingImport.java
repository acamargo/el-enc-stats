package ch.cern.statistics.service.lhclogging;

import ch.cern.statistics.entities.lhclogging.TagValue;

import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 17/5/17.
 */
public interface LhcLoggingImport {

    List<TagValue> importData(Date day, String tagName);

}
