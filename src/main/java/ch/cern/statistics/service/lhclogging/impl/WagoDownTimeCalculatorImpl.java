package ch.cern.statistics.service.lhclogging.impl;

import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.entities.lhclogging.TagValue;
import ch.cern.statistics.repositories.WagoDownTimeRepository;
import ch.cern.statistics.repositories.WagoRepository;
import ch.cern.statistics.service.lhclogging.GenericDownTimeCalculator;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;
import ch.cern.statistics.service.lhclogging.WagoDownTimeCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 18/5/17.
 */
public class WagoDownTimeCalculatorImpl extends GenericDownTimeCalculatorImpl<WagoDownTime>
        implements WagoDownTimeCalculator {

    public static final Logger logger = LoggerFactory.getLogger(WagoDownTimeCalculatorImpl.class);

    public static final String TAG_SUFFIX = "_OK";

    private WagoRepository wagoRepository;

    private WagoDownTimeRepository wagoDownTimeRepository;

    private LhcLoggingImport lhcLoggingImport;

    public WagoDownTimeCalculatorImpl(WagoRepository wagoRepository, WagoDownTimeRepository wagoDownTimeRepository,
                                      LhcLoggingImport lhcLoggingImport) {

        super(WagoDownTime.class, lhcLoggingImport);

        this.wagoRepository = wagoRepository;

        this.wagoDownTimeRepository = wagoDownTimeRepository;

        this.lhcLoggingImport = lhcLoggingImport;
    }

    @Override
    public List<WagoDownTime> importTags(Date day, boolean persist) {

        logger.info("Importing tags from LHC Logging: " + day.toString());

        List<WagoDownTime> wagoDownTimes = importTags(day, getTagNames());

        persistIfRequired(wagoDownTimes, persist);

        return wagoDownTimes;

    }

    @Override
    public List<WagoDownTime> bulkImport(Date startDate, Date endDate, boolean persist) {

        logger.info("Starting bulk import from LHC Logging: " + startDate.toString() + " to " + endDate.toString());

        List<WagoDownTime> wagoDownTimes =  bulkImport(startDate, endDate, getTagNames());

        persistIfRequired(wagoDownTimes, persist);

        return wagoDownTimes;

    }

    @Override
    public List<WagoDownTime> importYesterdaysTags(boolean persist) {

        logger.info("Starting yesterday's tags import from LHC Logging");

        List<WagoDownTime> wagoDownTimes =  importYesterdaysTags(getTagNames());

        persistIfRequired(wagoDownTimes, persist);

        return wagoDownTimes;

    }

    private List<String> getTagNames() {

        return wagoRepository.findAll()
                .stream()
                .map(w -> w.getName() + TAG_SUFFIX)
                .collect(Collectors.toList());

    }

    private void persistIfRequired(List<WagoDownTime> wagoDownTimes, boolean persist) {

        if (wagoDownTimes.size() == 0) logger.info("No down times found. Nothing will be inserted.");

        else if (persist) wagoDownTimeRepository.save(wagoDownTimes);

    }

}
