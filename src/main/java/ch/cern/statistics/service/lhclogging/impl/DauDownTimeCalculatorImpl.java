package ch.cern.statistics.service.lhclogging.impl;

import ch.cern.statistics.entities.DauDownTime;
import ch.cern.statistics.repositories.DauDownTimeRepository;
import ch.cern.statistics.repositories.DauRepository;
import ch.cern.statistics.service.lhclogging.DauDownTimeCalculator;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
public class DauDownTimeCalculatorImpl extends GenericDownTimeCalculatorImpl<DauDownTime>
        implements DauDownTimeCalculator {

    private static final Logger logger = LoggerFactory.getLogger(DauDownTimeCalculatorImpl.class);

    public static final String TAG_SUFFIX = "_OK";

    private DauRepository dauRepository;

    private DauDownTimeRepository dauDownTimeRepository;

    private LhcLoggingImport lhcLoggingImport;

    public DauDownTimeCalculatorImpl(LhcLoggingImport lhcLoggingImport,
                                     DauRepository dauRepository, DauDownTimeRepository dauDownTimeRepository,
                                     LhcLoggingImport lhcLoggingImport1) {

        super(DauDownTime.class, lhcLoggingImport);

        this.dauRepository = dauRepository;

        this.dauDownTimeRepository = dauDownTimeRepository;

        this.lhcLoggingImport = lhcLoggingImport1;

    }

    @Override
    public List<DauDownTime> importTags(Date day, boolean persist) {

        logger.info("Importing tags from LHC Logging: " + day.toString());

        List<DauDownTime> dauDownTimes = importTags(day, getTagNames());

        persistIfRequired(dauDownTimes, persist);

        return dauDownTimes;

    }

    @Override
    public List<DauDownTime> bulkImport(Date startDate, Date endDate, boolean persist) {

        logger.info("Starting bulk import from LHC Logging: " + startDate.toString() + " to " + endDate.toString());

        List<DauDownTime> dauDownTimes =  bulkImport(startDate, endDate, getTagNames());

        persistIfRequired(dauDownTimes, persist);

        return dauDownTimes;

    }

    @Override
    public List<DauDownTime> importYesterdaysTags(boolean persist) {

        logger.info("Starting yesterday's tags import from LHC Logging");

        List<DauDownTime> dauDownTimes =  importYesterdaysTags(getTagNames());

        persistIfRequired(dauDownTimes, persist);

        return dauDownTimes;

    }

    private List<String> getTagNames() {

        return dauRepository.findAll()
                .stream()
                .map(w -> w.getName() + TAG_SUFFIX)
                .collect(Collectors.toList());

    }

    private void persistIfRequired(List<DauDownTime> dauDownTimes, boolean persist) {

        if (dauDownTimes.size() == 0) logger.info("No down times found. Nothing will be inserted.");

        else if (persist) dauDownTimeRepository.save(dauDownTimes);

    }


}
