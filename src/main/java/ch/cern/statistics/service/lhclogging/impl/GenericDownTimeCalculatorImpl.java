package ch.cern.statistics.service.lhclogging.impl;

import ch.cern.statistics.entities.DownTime;
import ch.cern.statistics.entities.lhclogging.TagValue;
import ch.cern.statistics.service.lhclogging.GenericDownTimeCalculator;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by alejandrocamargo on 26/5/17.
 */
public class GenericDownTimeCalculatorImpl<T extends DownTime> implements GenericDownTimeCalculator<T> {

    public static final Logger logger = LoggerFactory.getLogger(GenericDownTimeCalculatorImpl.class);

    private Class<T> clazz;

    private LhcLoggingImport lhcLoggingImport;

    public GenericDownTimeCalculatorImpl(Class<T> clazz, LhcLoggingImport lhcLoggingImport) {

        this.clazz = clazz;

        this.lhcLoggingImport = lhcLoggingImport;

    }

    /**
     * Takes a list of serialized objets coming from the Lhc Logging REST
     * Service and calculates and returns the detected downtimes.
     *
     * @param tagValues retrieved from LHLogging to be analyzed
     * @return list of down times
     */
    @Override
    public List<T> calculateDownTimes(List<TagValue> tagValues) {

        List<T> downTimes = new ArrayList<>();

        int startDownTime = -1;

        for (int i = 0; i < tagValues.size(); i++) {

            if (tagValues.get(i).getValue() == 0 && startDownTime == -1) {

                startDownTime = i;

            }

            if (startDownTime != -1) {

                if (tagValues.get(i).getValue() == 1) {

                    downTimes.add(mapDownTime(tagValues.get(startDownTime), tagValues.get(i)));

                    startDownTime = -1;

                }

            }

        }

        return downTimes;

    }

    private T mapDownTime(TagValue start, TagValue end) {

        try {

            T dt = clazz.newInstance();

            dt.setDeviceName(start.getTagName().split("\\_")[0]);

            dt.setEndTime(end.getDate());

            dt.setStartTime(start.getDate());

            logger.info("Down Time found: " + dt.toString());

            return dt;

        } catch (IllegalAccessException | InstantiationException e) {

            logger.error("Cannot map DownTime to " + clazz.getName());

        }

        return null;

    }

    /**
     * Imports all device tag values defined in the T document
     * and calculates the downtimes for the given day.
     * If persist == true, it saves the results in the
     * T_DOWNTIME document.
     *
     * @param day to import
     * @return list of down times
     */
    public List<T> importTags(Date day, List<String> tagNames) {

        List<T> wagoDownTimes = new ArrayList<>();

        for (String tag : tagNames) {

            List<TagValue> tagValues = lhcLoggingImport.importData(day, tag);

            if (tagValues.size() == 0) logger.debug("No values: " + tag);

            else wagoDownTimes.addAll(calculateDownTimes(tagValues));

        }

        return wagoDownTimes;

    }

    /**
     * Imports all device tag values defined in the T document
     * and calculates the downtimes for yesterday.
     * If persist == true, it saves the results in the
     * T_DOWNTIME document.
     *
     * @param startDate from the interval to import
     * @param endDate from the interval to import
     * @return list of down times
     */
    public List<T> bulkImport(Date startDate, Date endDate, List<String> tagNames) {

        List<T> dtLs = new ArrayList<>();

        Calendar cal = Calendar.getInstance();

        cal.setTime(startDate);

        while (cal.getTime().before(endDate)) {

            cal.add(Calendar.DATE, 1);

            dtLs.addAll(importTags(cal.getTime(), tagNames));

        }

        return dtLs;

    }

    /**
     * Imports all devices tag values defined in the T document
     * and calculates the downtimes for yesterday.
     * If persist == true, it saves the results in the
     * T_DOWNTIME document.
     *
     * @return list of down times
     */
    public List<T> importYesterdaysTags(List<String> tagNames) {

        final Calendar cal = Calendar.getInstance();

        cal.add(Calendar.DATE, -1);

        return importTags(cal.getTime(), tagNames);

    }


}
