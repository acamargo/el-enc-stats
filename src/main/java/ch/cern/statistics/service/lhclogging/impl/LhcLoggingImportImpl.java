package ch.cern.statistics.service.lhclogging.impl;

import ch.cern.statistics.entities.lhclogging.ImportInformation;
import ch.cern.statistics.entities.lhclogging.TagValue;
import ch.cern.statistics.service.lhclogging.LhcLoggingImport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Date;
import java.util.List;



/**
 * Created by alejandrocamargo on 17/5/17.
 */
public class LhcLoggingImportImpl implements LhcLoggingImport {

    public static final Logger logger = LoggerFactory.getLogger(LhcLoggingImportImpl.class);

    @Value("${lhclogging.url}")
    private String lhcLoggingUrl;

    private RestTemplate restTemplate;

    public LhcLoggingImportImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<TagValue> importData(Date day, String tagName) {

        HttpEntity<ImportInformation> request = new HttpEntity<>(new ImportInformation(day, tagName));

        logger.debug("POST object: " + request.getBody().toString());

        ResponseEntity<List<TagValue>> response = restTemplate
                .exchange(lhcLoggingUrl, HttpMethod.POST, request, new ParameterizedTypeReference<List<TagValue>>(){});

        Collections.sort(response.getBody());

        return response.getBody();

    }

}
