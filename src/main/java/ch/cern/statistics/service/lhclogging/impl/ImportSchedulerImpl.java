package ch.cern.statistics.service.lhclogging.impl;

import ch.cern.statistics.service.lhclogging.ImportScheduler;
import ch.cern.statistics.service.lhclogging.WagoDownTimeCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by alejandrocamargo on 19/5/17.
 */
public class ImportSchedulerImpl implements ImportScheduler {

    private static final Logger logger = LoggerFactory.getLogger(ImportSchedulerImpl.class);

    private WagoDownTimeCalculator wagoDownTimeCalculator;

    public ImportSchedulerImpl(WagoDownTimeCalculator wagoDownTimeCalculator) {
        this.wagoDownTimeCalculator = wagoDownTimeCalculator;
    }

    /**
     * Triggers the process at 00:45h everyday
     */
    @Scheduled(cron = "0 45 0 * * ?")
    public void importWagoTagsAndCalculateDownTimes() {

        logger.info("Starting scheduled task for Wago devices! ");

        wagoDownTimeCalculator.importYesterdaysTags(true);

    }


}
