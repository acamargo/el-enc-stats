package ch.cern.statistics.service.charts.impl;

import ch.cern.statistics.dto.EamRtuInterventionsPerYear;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.MonthEnum;
import ch.cern.statistics.service.EamService;
import ch.cern.statistics.service.charts.CoPiquetChartService;
import ch.cern.statistics.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by acamargo on 07/02/2017.
 */
public class CoPiquetChartServiceImpl implements CoPiquetChartService {

    private EamService eamService;

    public CoPiquetChartServiceImpl(EamService eamService) {
        this.eamService = eamService;
    }

    public List<Serie<Integer>> getRtuByYearBarChartSeries(Integer year, String piquetCode) {

        List<Serie<Integer>> seriesList = new ArrayList<>();

        List<EamRtuInterventionsPerYear>  ls = eamService.getInterventionsByRtuByYear(year, piquetCode);

        ls.forEach(e -> {

            Serie<Integer> serie = new Serie<>();

            serie.setName(e.getEvtObject());

            serie.addData(e.getTimes());

            seriesList.add(serie);

        });

        return seriesList;
    }

    public List<PieChartSerie> getTotalCoPiquetRtuRelatedSeries(Integer year) {

        final List<PieChartSerie> serieList = new ArrayList<>();

        AtomicInteger totalInterventions = new AtomicInteger(0);

        AtomicInteger rtuRelatedInterventions = new AtomicInteger(0);

        eamService.getCoPiquetsByYear(year)
                .entrySet()
                .forEach(e -> totalInterventions.addAndGet(e.getValue()));

        Utils.getCoPiquets()
                .forEach(p -> eamService.getInterventionsByRtuByYear(year, p.toString())
                        .forEach(e -> rtuRelatedInterventions.addAndGet(e.times)));

        PieChartSerie serie =  new PieChartSerie();

        serie.setName("Interventions");
        serie.addNewSerie("Other", new Double(totalInterventions.get() - rtuRelatedInterventions.get()));
        serie.addNewSerie("Rtu related", new Double(rtuRelatedInterventions.get()));

        serieList.add(serie);

        return serieList;

    }

    public List<PieChartSerie> getCoPiquetRturelatedSeriesByCode(Integer year, CoPiquetEnum piquet) {

        final List<PieChartSerie> serieList = new ArrayList<>();

        Integer totalPiquets = eamService.getCoPiquetsByYear(year).get(piquet);

        AtomicInteger rtuRelated = new AtomicInteger();

        eamService.getInterventionsByRtuByYear(year, piquet.toString())
                .forEach(e -> rtuRelated.addAndGet(e.getTimes()));

        PieChartSerie serie =  new PieChartSerie();

        serie.setName("Interventions");
        serie.addNewSerie("Other", new Double(totalPiquets - rtuRelated.get()));
        serie.addNewSerie("Rtu related", new Double(rtuRelated.get()));

        serieList.add(serie);

        return serieList;

    }

    public List<Serie> getPiquetSeriesBarChart(Integer year) {

        List<Serie> series = new ArrayList<>();

        Utils.getCoPiquets()
                .forEach(p -> series.add(getSerieBarChartFromMap(eamService.getRtuPiquetsByMonth(year, p.toString()),
                        p.toString())));

        return series;

    }


    private Serie<Integer> getSerieBarChartFromMap(Map<MonthEnum, List<EamEvent>> map, String piquetName) {

        Serie<Integer> s = new Serie<>();

        s.addData(map.get(MonthEnum.JANUARY).size());
        s.addData(map.get(MonthEnum.FEBRUARY).size());
        s.addData(map.get(MonthEnum.MARCH).size());
        s.addData(map.get(MonthEnum.APRIL).size());
        s.addData(map.get(MonthEnum.MAY).size());
        s.addData(map.get(MonthEnum.JUNE).size());
        s.addData(map.get(MonthEnum.JULY).size());
        s.addData(map.get(MonthEnum.AUGUST).size());
        s.addData(map.get(MonthEnum.SEPTEMBER).size());
        s.addData(map.get(MonthEnum.OCTOBER).size());
        s.addData(map.get(MonthEnum.NOVEMBER).size());
        s.addData(map.get(MonthEnum.DECEMBER).size());

        s.setName(piquetName);

        return s;

    }


}
