package ch.cern.statistics.service.charts;

import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.MonthEnum;

import java.util.List;
import java.util.Map;

/**
 * Created by acamargo on 07/02/2017.
 */
public interface CoPiquetChartService {

    List<Serie<Integer>> getRtuByYearBarChartSeries(Integer year, String piquetCode);

    List<PieChartSerie> getTotalCoPiquetRtuRelatedSeries(Integer year);

    List<PieChartSerie> getCoPiquetRturelatedSeriesByCode(Integer year, CoPiquetEnum piquet);

    List<Serie> getPiquetSeriesBarChart(Integer year);


}
