package ch.cern.statistics.service.charts;

import ch.cern.statistics.entities.HeatMapSerie;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;

import java.util.List;


public interface RtuDownTimeChartService {

    List<Serie> getNumberRtuDownTimesPerYear(List<String> rtuName, Integer year);

    List<PieChartSerie> getYearlyDownTimePerRtuPieChart(String rtuHostName, Integer year);

    HeatMapSerie getRtuHeatMapCount(String rtuHostName, Integer year);

    HeatMapSerie getRtuHeatMapCountAllRtus(Integer year);

}
