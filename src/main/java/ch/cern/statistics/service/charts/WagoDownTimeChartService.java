package ch.cern.statistics.service.charts;

import ch.cern.statistics.entities.DownTimeStatistics;
import ch.cern.statistics.entities.PieChartSerie;

import java.util.List;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public interface WagoDownTimeChartService {

    List<String> getWagoNames();

    List<PieChartSerie> getYearlyDownTimePerWagoPieChart(String wagoName, Integer year);

    DownTimeStatistics getWagoDownTimeStatistics(String wagoName, Integer year);


}
