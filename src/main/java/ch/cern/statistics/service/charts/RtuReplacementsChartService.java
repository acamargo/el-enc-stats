package ch.cern.statistics.service.charts;

import ch.cern.statistics.entities.Serie;

import java.util.List;

/**
 * Created by alejandrocamargo on 19/3/17.
 */
public interface RtuReplacementsChartService {

    List<Serie> getReplacementsByYearSeries();

    List<Serie> getReplacementsByYearByRtuTypeSeries();
}
