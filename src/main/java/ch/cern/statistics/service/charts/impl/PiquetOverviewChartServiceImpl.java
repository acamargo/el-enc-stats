package ch.cern.statistics.service.charts.impl;


import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.MonthEnum;
import ch.cern.statistics.entities.enums.OpPiquetEnum;
import ch.cern.statistics.service.EamService;
import ch.cern.statistics.service.charts.PiquetOverviewChartService;
import ch.cern.statistics.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


public class PiquetOverviewChartServiceImpl implements PiquetOverviewChartService {

    private EamService eamService;

    public PiquetOverviewChartServiceImpl(EamService eamService) {
        this.eamService = eamService;
    }


    public List<Serie> getPiquetSeriesBarChart(Integer year) {

        List<Serie> seriesList = new ArrayList<>();

        eamService.getCoPiquetsByYear(year)
                .entrySet()
                .stream()
                .forEach(e -> {
                    Serie<Integer> s = new Serie<>();
                    s.setName(e.getKey().toString());
                    s.addData(e.getValue());

                    seriesList.add(s);
                });

        Map<OpPiquetEnum, Integer> map = eamService.getOpPiquetsByYear(year);

        Utils.getOpPiquets().forEach(e -> {

            Serie<Integer> s = new Serie<>();
            s.setName(e.toString());
            s.addData(map.get(e));

            seriesList.add(s);
        });

        return seriesList;
    }

    public List<PieChartSerie> getPiquetSeriesPieChart(Integer year) {

        List<PieChartSerie> seriesList = new ArrayList<>();

        Map<CoPiquetEnum, Integer> coPiquetEnumIntegerMap = eamService.getCoPiquetsByYear(year);

        Map<OpPiquetEnum, Integer> opPiquetEnumIntegerMap = eamService.getOpPiquetsByYear(year);

        AtomicInteger totalPiquets = new AtomicInteger(0);

        coPiquetEnumIntegerMap
                .entrySet()
                .stream()
                .forEach(e -> totalPiquets.addAndGet(e.getValue()));

        opPiquetEnumIntegerMap
                .entrySet()
                .stream()
                .forEach(e -> totalPiquets.addAndGet(e.getValue()));

        PieChartSerie serie = new PieChartSerie();

        serie.setName("Value: ");

        coPiquetEnumIntegerMap
                .entrySet()
                .stream()
                .forEach(e -> {

                    serie.addNewSerie(e.getKey().toString(), ((double)e.getValue() / totalPiquets.get()) * 100);

                    seriesList.add(serie);

                });

        Utils.getOpPiquets().forEach(e -> {

            serie.addNewSerie(e.toString(), ((double)opPiquetEnumIntegerMap.get(e) / totalPiquets.get()) * 100);

            seriesList.add(serie);
        });

        return seriesList;

    }

    public List<Serie> getPiquetSeriesLineChart(Integer year) {

        List<Serie> series = new ArrayList<>();

        Utils.getPiquets()
                .forEach(p ->  series.add(getSerieLineChartFromMap(eamService.getPiquetsByMonth(year, p), p)));

        return series;

    }

    private Serie<Integer> getSerieLineChartFromMap(Map<MonthEnum, List<EamEvent>> map, String piquetName) {

        Serie<Integer> s = new Serie<>();

        s.addData(map.get(MonthEnum.JANUARY).size());
        s.addData(map.get(MonthEnum.FEBRUARY).size());
        s.addData(map.get(MonthEnum.MARCH).size());
        s.addData(map.get(MonthEnum.APRIL).size());
        s.addData(map.get(MonthEnum.MAY).size());
        s.addData(map.get(MonthEnum.JUNE).size());
        s.addData(map.get(MonthEnum.JULY).size());
        s.addData(map.get(MonthEnum.AUGUST).size());
        s.addData(map.get(MonthEnum.SEPTEMBER).size());
        s.addData(map.get(MonthEnum.OCTOBER).size());
        s.addData(map.get(MonthEnum.NOVEMBER).size());
        s.addData(map.get(MonthEnum.DECEMBER).size());

        s.setName(piquetName);

        return s;

    }

}
