package ch.cern.statistics.service.charts.impl;

import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.Year;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.repositories.YearRepository;
import ch.cern.statistics.service.EamService;
import ch.cern.statistics.service.charts.RtuReplacementsChartService;
import ch.cern.statistics.util.DateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 19/3/17.
 */
public class RtuReplacementsChartServiceImpl implements RtuReplacementsChartService {

    private EamService eamService;

    private YearRepository yearRepository;

    //Autowired
    public RtuReplacementsChartServiceImpl(EamService eamService, YearRepository yearRepository) {
        this.eamService = eamService;
        this.yearRepository = yearRepository;
    }

    public List<Serie> getReplacementsByYearSeries() {

        final List<Year> years = yearRepository.findYearsBetweenDates(2014, DateUtil.getCurrentYear());

        final List<Serie> series = new ArrayList<>();

        eamService.getCountAllRtuReplacementsByYear(years)
                .entrySet()
                .stream()
                .forEach(e -> {

                    Serie<Integer> serie = new Serie<>();

                    serie.addData(e.getValue());
                    serie.setName(e.getKey().toString());

                    series.add(serie);

                });


        return series;
    }

    public List<Serie> getReplacementsByYearByRtuTypeSeries() {

        final List<Year> years = yearRepository.findYearsBetweenDates(2014, DateUtil.getCurrentYear());

        final List<Integer> advantechValues = new ArrayList<>();

        final List<Integer> clp500Values = new ArrayList<>();

        final List<Integer> clp500eValues = new ArrayList<>();

        final List<Serie> series = new ArrayList<>();

        for (Map.Entry<Integer, List<EamEvent>> e: eamService.getAllRtuReplacementsByYear(years).entrySet()) {

            Long count = e.getValue()
                    .stream()
                    .filter(a -> a.getEvtDesc().contains("EDP*ADVAUC500"))
                    .count();

            advantechValues.add(count.intValue());

            count = e.getValue()
                    .stream()
                    .filter(a -> a.getEvtDesc().contains("EDP*EFACCLP5*"))
                    .count();

            clp500Values.add(count.intValue());


            count = e.getValue()
                    .stream()
                    .filter(a -> a.getEvtDesc().contains("EDP*EFACCLP5E"))
                    .count();

            clp500eValues.add(count.intValue());
        }

        Serie advantech = new Serie();

        advantechValues.forEach(e -> advantech.addData(e));

        advantech.setName("ADVANTECH");

        series.add(advantech);


        Serie clp500 = new Serie();

        clp500Values.forEach(e -> clp500.addData(e));

        clp500.setName("EFACEC CLP500");

        series.add(clp500);


        Serie clp500e = new Serie();

        clp500eValues.forEach(e -> clp500e.addData(e));

        clp500e.setName("EFACEC CLP500E");

        series.add(clp500e);


        return series;


    }

}
