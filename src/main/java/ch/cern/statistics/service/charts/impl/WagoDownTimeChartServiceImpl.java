package ch.cern.statistics.service.charts.impl;

import ch.cern.statistics.entities.DownTimeStatistics;
import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.repositories.WagoDownTimeRepository;
import ch.cern.statistics.service.WagoDownTimeService;
import ch.cern.statistics.service.charts.WagoDownTimeChartService;
import ch.cern.statistics.util.DateUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public class WagoDownTimeChartServiceImpl implements WagoDownTimeChartService {


    private WagoDownTimeService wagoDownTimeService;

    private WagoDownTimeRepository wagoDownTimeRepository;

    public WagoDownTimeChartServiceImpl(WagoDownTimeService wagoDownTimeService,
                                        WagoDownTimeRepository wagoDownTimeRepository) {

        this.wagoDownTimeService = wagoDownTimeService;
        this.wagoDownTimeRepository = wagoDownTimeRepository;
    }

    public List<String> getWagoNames() {

        List<String> ls = wagoDownTimeRepository.getWagoNames();

        Collections.sort(ls);

        return ls;
    }

    public List<PieChartSerie> getYearlyDownTimePerWagoPieChart(String wagoName, Integer year) {

        wagoName = wagoName.replace("_", "/");

        AtomicLong sum = new AtomicLong(0L);

        wagoDownTimeService.findDownTimesByWagoBetweenDates(wagoName, DateUtil.getDate("01-01-" + year + " 00:00:00"),
                DateUtil.getDate("31-12-" + year + " 23:59:59"))
                .stream()
                .forEach(e -> sum.addAndGet(e.downTimeInSeconds()));


        PieChartSerie serie = new PieChartSerie();

        serie.setName(wagoName);

        serie.addNewSerie("Downtime", sum.doubleValue());

        serie.addNewSerie("Uptime", new Double(DateUtil.SECONDS_IN_YEAR - sum.longValue()));

        List<PieChartSerie> ls = new ArrayList<>();

        ls.add(serie);

        return ls;

    }

    public DownTimeStatistics getWagoDownTimeStatistics(String wagoName, Integer year) {

        DownTimeStatistics stats = new DownTimeStatistics();

        wagoName = wagoName.replace("_", "/");

        AtomicLong totalDownTimeInSeconds = new AtomicLong(0L);


        List<WagoDownTime> ls = wagoDownTimeService.findDownTimesByWagoBetweenDates(wagoName,
                DateUtil.getDate("01-01-" + year + " 00:00:00"), DateUtil.getDate("31-12-" + year + " 23:59:59"));

        ls.stream().forEach(e -> totalDownTimeInSeconds.addAndGet(e.downTimeInSeconds()));

        stats.setTotalDownTimeInSeconds(totalDownTimeInSeconds.longValue());

        stats.setTotalDownTimes(ls.stream().count());

        stats.calculateAverageDownTime();


        return stats;

    }
}
