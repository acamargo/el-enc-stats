package ch.cern.statistics.service.charts;

import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;

import java.util.List;

/**
 * Created by alejandrocamargo on 10/3/17.
 */
public interface FaultChartService {

    List<Serie> getMajorFaultsAnualLineChart(Integer startYear, Integer endYear);

    List<Serie> getMinorFaultsAnualLineChart(Integer startYear, Integer endYear);

    List<Serie> getMajorFaultsBarChart(Integer year);

    List<Serie> getMinorFaultsBarChart(Integer year);

    List<Fault> getFaultsDataTable(Integer year);

    List<MajorFault> getMajorFaultsDataTable(Integer year);

}
