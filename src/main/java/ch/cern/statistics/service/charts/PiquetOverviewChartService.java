package ch.cern.statistics.service.charts;


import ch.cern.statistics.entities.PieChartSerie;
import ch.cern.statistics.entities.Serie;

import java.util.List;

public interface PiquetOverviewChartService {

    List<Serie> getPiquetSeriesBarChart(Integer year);

    List<PieChartSerie> getPiquetSeriesPieChart(Integer year);

    List<Serie> getPiquetSeriesLineChart(Integer year);



}
