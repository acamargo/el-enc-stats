package ch.cern.statistics.service.charts.impl;

import ch.cern.statistics.entities.Serie;
import ch.cern.statistics.entities.enums.MonthEnum;
import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;
import ch.cern.statistics.service.LogbookService;
import ch.cern.statistics.service.charts.FaultChartService;
import ch.cern.statistics.util.DateUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 10/3/17.
 */
public class FaultChartServiceImpl implements FaultChartService {

    private LogbookService logbookService;

    public FaultChartServiceImpl(LogbookService logbookService) {
        this.logbookService = logbookService;
    }


    public List<Serie> getMajorFaultsAnualLineChart(Integer startYear, Integer endYear) {

        List<Serie> seriesList = new ArrayList<>();

        List<MajorFault> faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum.EL);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum.EL.toString(), faults, startYear, endYear));

        faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum.OP);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum.OP.toString(), faults, startYear, endYear));

        faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum .ENC);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum .ENC.toString(), faults, startYear, endYear));

        return seriesList;

    }

    public List<Serie> getMinorFaultsAnualLineChart(Integer startYear, Integer endYear) {

        List<Serie> seriesList = new ArrayList<>();

        List<Fault> faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.EL);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum.EL.toString(), faults, startYear, endYear));

        faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.OP);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum.OP.toString(), faults, startYear, endYear));

        faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.ENC);

        seriesList.add(getCountPerYearMap(AbstractFault.OriginUnitEnum.ENC.toString(), faults, startYear, endYear));

        return seriesList;

    }

    public List<Serie> getMajorFaultsBarChart(Integer year) {


        List<Serie> seriesList = new ArrayList<>();

        List<MajorFault> faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum.EL);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.EL.toString()));

        faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum.OP);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.OP.toString()));

        faults = logbookService.getMajorFaults(AbstractFault.OriginUnitEnum.ENC);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.ENC.toString()));

        return seriesList;

    }

    public List<Serie> getMinorFaultsBarChart(Integer year) {


        List<Serie> seriesList = new ArrayList<>();

        List<Fault> faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.EL);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.EL.toString()));

        faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.OP);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.OP.toString()));

        faults = logbookService.getFaults(AbstractFault.OriginUnitEnum.ENC);

        seriesList.add(getMonthlySerie(year, faults, AbstractFault.OriginUnitEnum.ENC.toString()));

        return seriesList;

    }


    private Serie getCountPerYearMap(String serieName, List<? extends AbstractFault> faults, int startYear,
                                     int endYear) {

        Map<Integer, Long> map = new TreeMap<>();

        int year = startYear;

        while (year <= endYear) {

            Date start = DateUtil.getDate("01-01-" + year + " 00:00:00");
            Date end = DateUtil.getDate("31-12-" + year + " 23:59:59");



            Long count = faults
                    .stream()
                    .filter(m -> m.getCreated().after(start) && m.getCreated().before(end))
                    .count();

            map.put(year, count);

            year++;
        }

        Serie<Long> s = new Serie();

        s.setName(serieName);

        map.entrySet().forEach(e -> s.addData(e.getValue()));


        return s;

    }

    private Serie getMonthlySerie(Integer year, List<? extends AbstractFault> faults, String serieName) {

        Map<MonthEnum, List<? extends AbstractFault>> map = filterFaultListByMonth(year, faults);

        Serie<Integer> s = new Serie<>();

        s.addData(map.get(MonthEnum.JANUARY).size());
        s.addData(map.get(MonthEnum.FEBRUARY).size());
        s.addData(map.get(MonthEnum.MARCH).size());
        s.addData(map.get(MonthEnum.APRIL).size());
        s.addData(map.get(MonthEnum.MAY).size());
        s.addData(map.get(MonthEnum.JUNE).size());
        s.addData(map.get(MonthEnum.JULY).size());
        s.addData(map.get(MonthEnum.AUGUST).size());
        s.addData(map.get(MonthEnum.SEPTEMBER).size());
        s.addData(map.get(MonthEnum.OCTOBER).size());
        s.addData(map.get(MonthEnum.NOVEMBER).size());
        s.addData(map.get(MonthEnum.DECEMBER).size());

        s.setName(serieName);

        return s;

    }

    private Map<MonthEnum, List<? extends AbstractFault>> filterFaultListByMonth(
            Integer year, List<? extends AbstractFault> faultList) {

        Map<MonthEnum, List<? extends AbstractFault>> map = new HashMap<>();

        map.put(MonthEnum.JANUARY,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-01-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-02-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.FEBRUARY,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-02-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-03-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.MARCH,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-03-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-04-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.APRIL,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-04-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-05-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.MAY,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-05-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-06-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.JUNE,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-06-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-07-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.JULY,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-07-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-08-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.AUGUST,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-08-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-09-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.SEPTEMBER,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-09-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-10-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.OCTOBER,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-10-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-11-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.NOVEMBER,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-11-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("01-12-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.DECEMBER,
                faultList
                        .stream()
                        .filter(p -> p.getCreated().after(DateUtil.getDate("01-12-" + year + " 00:00:00")))
                        .filter(p -> p.getCreated().before(DateUtil.getDate("31-12-" + year + " 23:59:59")))
                        .collect(Collectors.toList()));

        return map;

    }

    public List<Fault> getFaultsDataTable(Integer year) {

        return logbookService.getFaultsByYear(year);

    }

    public List<MajorFault> getMajorFaultsDataTable(Integer year) {

        return logbookService.getMajorFaultsByYear(year);

    }


}
