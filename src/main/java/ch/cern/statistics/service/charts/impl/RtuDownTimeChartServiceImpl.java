package ch.cern.statistics.service.charts.impl;


import ch.cern.statistics.entities.*;
import ch.cern.statistics.service.RtuDownTimeService;
import ch.cern.statistics.service.charts.RtuDownTimeChartService;
import ch.cern.statistics.util.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class RtuDownTimeChartServiceImpl implements RtuDownTimeChartService {

    public final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());


    private RtuDownTimeService rtuDownTimeService;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    //Autowired
    public RtuDownTimeChartServiceImpl(RtuDownTimeService rtuDownTimeService) {
        this.rtuDownTimeService = rtuDownTimeService;
    }


    public List<Serie> getNumberRtuDownTimesPerYear(List<String> rtuNames, Integer year) {

        List<Serie> rtuSeries = new ArrayList<>();

        rtuNames.forEach(rtu -> rtuSeries.add(getNumberRtuDownTimesPerYear(rtu, year)));

        return rtuSeries;
    }

    private Serie getNumberRtuDownTimesPerYear(String rtuName, Integer year) {

        rtuName = rtuName.replace("_", "/");

        Serie serie = new Serie();

        serie.setName(rtuName);

        Date start = DateUtil.getDate("01-01-" + year + " 00:00:00");
        Date end = DateUtil.getDate("31-12-" + year + " 23:59:59");

        List<RtuDownTime> downTimeList = rtuDownTimeService.findDownTimesByRtuBetweenDates(rtuName, start, end);

        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-01-" + year + " 00:00:00"),
                DateUtil.getDate("01-02-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-02-" + year + " 00:00:00"),
                DateUtil.getDate("01-03-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-03-" + year + " 00:00:00"),
                DateUtil.getDate("01-04-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil. getDate("01-04-" + year + " 00:00:00"),
                DateUtil.getDate("01-05-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-05-" + year + " 00:00:00"),
                DateUtil.getDate("01-06-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-06-" + year + " 00:00:00"),
                DateUtil.getDate("01-07-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-07-" + year + " 00:00:00"),
                DateUtil.getDate("01-08-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-08-" + year + " 00:00:00"),
                DateUtil.getDate("01-09-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-09-" + year + " 00:00:00"),
                DateUtil.getDate("01-10-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-10-" + year + " 00:00:00"),
                DateUtil.getDate("01-11-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-11-" + year + " 00:00:00"),
                DateUtil.getDate("01-12-" + year + " 00:00:00")));
        serie.getData().add(filterDownTimeList(downTimeList, DateUtil.getDate("01-12-" + year + " 00:00:00"),
                DateUtil.getDate("31-12-" + year + " 23:59:59")));

        return serie;

    }



    private Long filterDownTimeList(List<RtuDownTime> downTimeList, Date dateStart, Date dateEnd) {

        Integer value = downTimeList
                .stream()
                .filter(e -> e.getStartTime().after(dateStart))
                .filter(e -> e.getStartTime().before(dateEnd))
                .collect(Collectors.toList())
                .size();

        return value.longValue();

    }


    public List<PieChartSerie> getYearlyDownTimePerRtuPieChart(String rtuHostName, Integer year) {

        rtuHostName = rtuHostName.replace("_", "/");

        AtomicLong sum = new AtomicLong(0L);

        rtuDownTimeService.findDownTimesByRtuBetweenDates(rtuHostName, DateUtil.getDate("01-01-" + year + " 00:00:00"),
                DateUtil.getDate("31-12-" + year + " 23:59:59"))
                .stream()
                .forEach(e -> sum.addAndGet(e.downTimeInSeconds()));


        PieChartSerie serie = new PieChartSerie();

        serie.setName(rtuHostName);

        serie.addNewSerie("Downtime", sum.doubleValue());

        serie.addNewSerie("Uptime", new Double(DateUtil.SECONDS_IN_YEAR - sum.longValue()));

        List<PieChartSerie> ls = new ArrayList<>();

        ls.add(serie);

        return ls;

    }

    public HeatMapSerie getRtuHeatMapCount(String rtuHostName, Integer year) {

        rtuHostName = rtuHostName.replace("_", "/");

        List<RtuDownTime> rtuDownTimeList = rtuDownTimeService.findDownTimesByRtuBetweenDates(rtuHostName,
                DateUtil.getDate("01-01-" + year + " 00:00:00"), DateUtil.getDate("31-12-" + year + " 23:59:59"));

        return populateHeatMapSerie(rtuDownTimeList);

    }

    public HeatMapSerie getRtuHeatMapCountAllRtus(Integer year) {

        List<RtuDownTime> rtuDownTimeList = rtuDownTimeService.findDownTimesBetweenDates(
                DateUtil.getDate("01-01-" + year + " 00:00:00"), DateUtil.getDate("31-12-" + year + " 23:59:59"));

        return populateHeatMapSerie(rtuDownTimeList);

    }

    private HeatMapSerie populateHeatMapSerie(List<RtuDownTime> rtuDownTimeList) {

        HeatMapSerie serie = new HeatMapSerie();

        for (int day = 0; day < 7; day++) {

            List<RtuDownTime> filteredByDay = filterHeatMapByDay(day, rtuDownTimeList);

            for (int hour = 0; hour < 24; hour++) {

                List<RtuDownTime> filteredByHour = filterHeatMapByHour(hour, filteredByDay);

                HeatMapEntry hmHour = new HeatMapEntry((long)day, (long)hour, filteredByHour.stream().count());

                serie.pushEntry(hmHour);

            }

        }

        return serie;

    }


    private List<RtuDownTime> filterHeatMapByDay(Integer dayOfTheWeek, List<RtuDownTime> rtuDownTimeList) {

        return rtuDownTimeList
                .stream()
                .filter(e -> DateUtil.getDayOfTheWeekFromDate(e.getStartTime()) == dayOfTheWeek)
                .collect(Collectors.toList());

    }

    private List<RtuDownTime> filterHeatMapByHour(Integer hourOfDay, List<RtuDownTime> rtuDownTimeList) {

        return rtuDownTimeList
                .stream()
                .filter(e -> DateUtil.getHourOfTheDayFromDate(e.getStartTime()) == hourOfDay)
                .collect(Collectors.toList());

    }
}
