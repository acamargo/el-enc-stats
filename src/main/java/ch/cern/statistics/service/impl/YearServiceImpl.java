package ch.cern.statistics.service.impl;

import ch.cern.statistics.repositories.YearRepository;
import ch.cern.statistics.service.YearService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 20/3/17.
 */
public class YearServiceImpl implements YearService {

    private YearRepository repository;

    public YearServiceImpl(YearRepository repository) {
        this.repository = repository;
    }

    public List<String> getYearListBetweenDates(Integer startYear, Integer endYear) {

        return repository.findYearsBetweenDates(startYear, endYear)
                .stream()
                .map(y -> y.getYearString())
                .collect(Collectors.toList());

    }


}
