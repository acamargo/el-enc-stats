package ch.cern.statistics.service.impl;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;
import ch.cern.statistics.service.LogbookService;
import ch.cern.statistics.service.logbook.LogBookRestClient;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 23/5/17.
 */
public class LogbookServiceRestImpl implements LogbookService {

    private final Logger logger = Logger.getLogger(LogbookServiceRestImpl.class);

    private LogBookRestClient restClient;

    public LogbookServiceRestImpl(LogBookRestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public List<Fault> getFaults(AbstractFault.OriginUnitEnum originUnit) {

        return restClient.getMinorFaults(originUnit);

    }

    @Override
    public List<MajorFault> getMajorFaults(AbstractFault.OriginUnitEnum originUnit) {

        return restClient.getMajorFaults(originUnit);
    }

    @Override
    public List<Fault> getFaultsByYear(Integer year) {

        Date start;

        Date end;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try {

            start = sdf.parse("01-01-" + year + " 00:00:00");

            end = sdf.parse("01-01-" + (year + 1) + " 00:00:00");


            return Arrays.asList(AbstractFault.OriginUnitEnum.values())
                    .parallelStream()
                    .flatMap(originUnit -> restClient.getMinorFaults(originUnit).stream())
                    .filter(e -> e.getCreated().after(start))
                    .filter(e -> e.getCreated().before(end))
                    .collect(Collectors.toList());

        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        return Collections.emptyList();

    }

    @Override
    public List<MajorFault> getMajorFaultsByYear(Integer year) {

        Date start;

        Date end;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try {

            start = sdf.parse("01-01-" + year + " 00:00:00");

            end = sdf.parse("01-01-" + (year + 1) + " 00:00:00");


            return Arrays.asList(AbstractFault.OriginUnitEnum.values())
                    .parallelStream()
                    .flatMap(originUnit -> restClient.getMajorFaults(originUnit).stream())
                    .filter(e -> e.getCreated().after(start))
                    .filter(e -> e.getCreated().before(end))
                    .collect(Collectors.toList());

        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        return Collections.emptyList();


    }
}
