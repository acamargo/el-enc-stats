package ch.cern.statistics.service.impl;

import ch.cern.statistics.entities.Event;
import ch.cern.statistics.entities.RtuDownTime;
import ch.cern.statistics.repositories.RtuDownTimeRepository;
import ch.cern.statistics.service.RtuDownTimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class RtuDownTimeServiceImpl implements RtuDownTimeService {

    public static final long RTU_DEPLOYMENT_TIME_SEC = 600;


    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    private RtuDownTimeRepository rtuDownTimeRepository;

    //Autowired
    public RtuDownTimeServiceImpl(RtuDownTimeRepository rtuDownTimeRepository) {
        this.rtuDownTimeRepository = rtuDownTimeRepository;
    }


    public List<RtuDownTime> findDownTimeByRtu(String rtuName) {

        return rtuDownTimeRepository.findByDeviceName(rtuName)
                .stream()
                .filter(e -> e.downTimeInSeconds() > RTU_DEPLOYMENT_TIME_SEC)
                .collect(Collectors.toList());

    }

    public List<RtuDownTime> findDownTimesByRtuBetweenDates(String rtuName, Date dateStart, Date dateEnd) {

        return rtuDownTimeRepository.findRtuBetweenDates(rtuName, dateStart, dateEnd)
                .stream()
                .filter(e -> e.downTimeInSeconds() > RTU_DEPLOYMENT_TIME_SEC)
                .collect(Collectors.toList());

    }

    public List<RtuDownTime> findDownTimesBetweenDates(Date dateStart, Date dateEnd) {

        return rtuDownTimeRepository.findBetweenDates(dateStart, dateEnd)
                .stream()
                .filter(e -> e.downTimeInSeconds() > RTU_DEPLOYMENT_TIME_SEC)
                .collect(Collectors.toList());

    }


}
