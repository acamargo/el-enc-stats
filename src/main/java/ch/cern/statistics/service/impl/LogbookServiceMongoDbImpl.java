package ch.cern.statistics.service.impl;

import ch.cern.statistics.entities.logbook.AbstractFault;
import ch.cern.statistics.entities.logbook.Fault;
import ch.cern.statistics.entities.logbook.MajorFault;
import ch.cern.statistics.repositories.FaultRepository;
import ch.cern.statistics.repositories.MajorFaultRepository;
import ch.cern.statistics.service.LogbookService;
import ch.cern.statistics.util.DateUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by alejandrocamargo on 2/3/17.
 */
public class LogbookServiceMongoDbImpl implements LogbookService {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    public enum FaultType {
        MAJOR_FAULT, MINOR_FAULT
    }

    private MajorFaultRepository majorFaultRepository;

    private FaultRepository faultRepository;

    public LogbookServiceMongoDbImpl(MajorFaultRepository majorFaultRepository, FaultRepository faultRepository) {
        this.majorFaultRepository = majorFaultRepository;
        this.faultRepository = faultRepository;
    }

    public List<MajorFault> getMajorFaults(AbstractFault.OriginUnitEnum originUnit) {

        return majorFaultRepository.getByOriginUnit(originUnit.toString());

    }

    public List<Fault> getFaults(AbstractFault.OriginUnitEnum  originUnit) {

        return faultRepository.getByOriginUnit(originUnit.toString());

    }

    public List<Fault> getFaultsByYear(Integer year) {

        Date start = DateUtil.getDate("01-01-" + year + " 00:00:00");
        Date end = DateUtil.getDate("31-12-" + year + " 23:59:59");

        return faultRepository.getBetweenDates(start, end);

    }

    public List<MajorFault> getMajorFaultsByYear(Integer year) {

        Date start = DateUtil.getDate("01-01-" + year + " 00:00:00");
        Date end = DateUtil.getDate("31-12-" + year + " 23:59:59");

        return majorFaultRepository.getBetweenDates(start, end);

    }

}
