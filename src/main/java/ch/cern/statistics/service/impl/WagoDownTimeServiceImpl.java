package ch.cern.statistics.service.impl;

import ch.cern.statistics.entities.WagoDownTime;
import ch.cern.statistics.repositories.WagoDownTimeRepository;
import ch.cern.statistics.service.WagoDownTimeService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alejandrocamargo on 7/4/17.
 */
public class WagoDownTimeServiceImpl implements WagoDownTimeService {

    public static final long FILTER_SEC = 0;


    public WagoDownTimeRepository wagoDownTimeRepository;

    public WagoDownTimeServiceImpl(WagoDownTimeRepository wagoDownTimeRepository) {
        this.wagoDownTimeRepository = wagoDownTimeRepository;
    }

    public List<WagoDownTime> findDownTimeByWago(String wagoName) {

        return wagoDownTimeRepository.findByDeviceName(wagoName)
                .stream()
                .filter(e -> e.downTimeInSeconds() > FILTER_SEC)
                .collect(Collectors.toList());

    }

    public List<WagoDownTime> findDownTimesByWagoBetweenDates(String wagoName, Date dateStart, Date dateEnd) {

        return wagoDownTimeRepository.findWagoBetweenDates(wagoName, dateStart, dateEnd)
                .stream()
                .filter(e -> e.downTimeInSeconds() > FILTER_SEC)
                .collect(Collectors.toList());

    }

    public List<WagoDownTime> findDownTimesBetweenDates(Date dateStart, Date dateEnd) {

        return wagoDownTimeRepository.findBetweenDates(dateStart, dateEnd)
                .stream()
                .filter(e -> e.downTimeInSeconds() > FILTER_SEC)
                .collect(Collectors.toList());

    }
}
