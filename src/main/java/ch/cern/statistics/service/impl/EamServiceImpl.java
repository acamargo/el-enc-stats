package ch.cern.statistics.service.impl;

import ch.cern.statistics.entities.Year;
import ch.cern.statistics.entities.eam.EamEvent;
import ch.cern.statistics.dto.EamRtuInterventionsPerYear;
import ch.cern.statistics.entities.enums.CoPiquetEnum;
import ch.cern.statistics.entities.enums.MonthEnum;
import ch.cern.statistics.entities.enums.OpPiquetEnum;
import ch.cern.statistics.repositories.EamJdbcRepository;
import ch.cern.statistics.service.EamService;
import ch.cern.statistics.util.DateUtil;

import java.util.*;
import java.util.stream.Collectors;

public class EamServiceImpl implements EamService {


    private EamJdbcRepository eamJdbcRepository;


    public EamServiceImpl(EamJdbcRepository eamJdbcRepository) {
        this.eamJdbcRepository = eamJdbcRepository;
    }

    public Map<OpPiquetEnum, Integer> getOpPiquetsByYear(Integer year) {

        Map<OpPiquetEnum, Integer> result = new HashMap<>();

        Arrays.asList(OpPiquetEnum.values()).forEach(p -> result.put(p,
                eamJdbcRepository.queryNumberPiquetByYear(year, p.toString())));

        return result;
    }

    public Map<CoPiquetEnum, Integer> getCoPiquetsByYear(Integer year) {

        Map<CoPiquetEnum, Integer> result = new HashMap<>();

        Arrays.asList(CoPiquetEnum.values()).forEach(p -> result.put(p,
                eamJdbcRepository.queryNumberPiquetByYear(year, p.toString())));

        return result;
    }


    public Map<MonthEnum, List<EamEvent>> getPiquetsByMonth(Integer year, String piquetCode) {

        List<EamEvent> eventList = eamJdbcRepository.getPiquetsByYearByCode(year, piquetCode);

        return filterEventListByMonth(year, eventList);

    }

    public Map<MonthEnum, List<EamEvent>> getRtuPiquetsByMonth(Integer year, String piquetCode) {

        List<EamEvent> eventList = eamJdbcRepository.getRtuRelatedInterventionsByYear(year, piquetCode);

        return filterEventListByMonth(year, eventList);

    }

    public List<EamRtuInterventionsPerYear> getInterventionsByRtuByYear(Integer year, String piquetCode) {

        return eamJdbcRepository.getRtuRelatedInterventionsByRtuByYear(year, piquetCode);

    }

    private Map<MonthEnum, List<EamEvent>> filterEventListByMonth(Integer year, List<EamEvent> eventList) {

        Map<MonthEnum, List<EamEvent>> map = new HashMap<>();

        map.put(MonthEnum.JANUARY,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-02-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.FEBRUARY,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-02-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-03-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.MARCH,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-03-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-04-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.APRIL,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-04-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-05-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.MAY,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-05-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-06-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.JUNE,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-06-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-07-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.JULY,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-07-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-08-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.AUGUST,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-08-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-09-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.SEPTEMBER,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-09-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-10-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.OCTOBER,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-10-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-11-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.NOVEMBER,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-11-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("01-12-" + year + " 00:00:00")))
                        .collect(Collectors.toList()));

        map.put(MonthEnum.DECEMBER,
                eventList
                        .stream()
                        .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-12-" + year + " 00:00:00")))
                        .filter(p -> p.getEvtDate().before(DateUtil.getDate("31-12-" + year + " 23:59:59")))
                        .collect(Collectors.toList()));

        return map;

    }

    public List<EamEvent> getRtuReplacementsByYear(String year) {

        return eamJdbcRepository.getAllRtuReplacements()
                .stream()
                .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-01-" + year + " 00:00:00")))
                .filter(p -> p.getEvtDate().before(DateUtil.getDate("31-12-" + year + " 23:59:59")))
                .collect(Collectors.toList());

    }

    /**
     * Returns a map with the years and the count of the replacements
     * @param yearList list of years
     * @return Map with year and value
     */
    public Map<Integer, Integer> getCountAllRtuReplacementsByYear(List<Year> yearList) {

        Map<Integer, Integer> map = new TreeMap<>();

        List<EamEvent> replacements =  eamJdbcRepository.getAllRtuReplacements();

        for (Year year : yearList) {

            Long count =
                    replacements.stream()
                    .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-01-" + year.getYearString() + " 00:00:00")))
                    .filter(p -> p.getEvtDate().before(DateUtil.getDate("31-12-" + year.getYearString() + " 23:59:59")))
                    .count();

            map.put(year.getYear(), count.intValue());
        }

        return map;

    }

    public Map<Integer, List<EamEvent>> getAllRtuReplacementsByYear(List<Year> yearList) {

        Map<Integer, List<EamEvent>> map = new TreeMap<>();

        List<EamEvent> replacements =  eamJdbcRepository.getAllRtuReplacements();

        for (Year year : yearList) {

            List<EamEvent> events =
                    replacements.stream()
                            .filter(p -> p.getEvtDate().after(DateUtil.getDate("01-01-" + year.getYearString()
                                    + " 00:00:00")))
                            .filter(p -> p.getEvtDate().before(DateUtil.getDate("31-12-" + year.getYearString()
                                    + " 23:59:59")))
                            .collect(Collectors.toList());

            map.put(year.getYear(), events);
        }

        return map;

    }


}
