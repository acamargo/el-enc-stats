/**
 * Created by alejandrocamargo on 16/3/17.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

    $('#rtu').val("All RTUs")

});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2014/' + (currentYear - 1), function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

    });

    $('#yearDropDown').dropdown({
        onChange: selectionPerformed,
    });

    $('#rtuDropDown').dropdown({
        onChange: selectionPerformed,
    });

    getRtusForDropDown()

    selectionPerformed()

}

function getRtusForDropDown() {

    $.get('/api/rtus/dropDown/', function (response) {
        var json = response

        var list = $("#rtuDropDownList")

        json.results.forEach(function(e) {

            list.append('<div class="item" data-value="' + e.value + '">' + e.name + '</div>')

        })

        $("#rtuDropDown").dropdown("refresh");

        $('#rtuDropDown').dropdown('set selected', 'All RTUs')

    })

}

function selectionPerformed() {

    $('#heatmaploader').dimmer('show');

    var year = $('#year').val();

    var rtu = $('#rtu').val();

    if (rtu === 'All RTUs') rtu = 'ALL_RTU'

    $.get('/api/rtuDownTime/heatMap/' + rtu + '/' + year, function (response) {
        var json = response

        drawHeatmap(json, 'heatmap', rtu, year)
        $('#heatmaploader').dimmer('hide');

    })

}

function drawHeatmap(json, destinationDiv, rtuName, year) {

    if (rtuName === 'ALL_RTU') rtuName = 'All RTUs'

    rtuName = rtuName.replace('_', '/')

    Highcharts.chart(destinationDiv, {

        chart: {
            type: 'heatmap',
            marginTop: 40,
            marginBottom: 80,
            plotBorderWidth: 1
        },


        title: {
            text: rtuName + ' Down times per day/hour (' + year + ')'
        },


        xAxis: {

            categories: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00','16:00','17:00', '18:00','19:00', '20:00', '21:00', '22:00', '23:00'],
            title: 'Time'


        },

        yAxis: {
            categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            title: 'Day of the week'

        },

        colorAxis: {
            min: 0,
            minColor: 'white',
            maxColor: Highcharts.getOptions().colors[0]
        },

        legend: {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 25,
            symbolHeight: 280
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.yAxis.categories[this.point.y] + ' at ' + this.series.xAxis.categories[this.point.x] + 'h: ' +  this.point.value  + ' time(s) </b>';
            }
        },

        series: [{
            name: 'RTU down times',
            borderWidth: 1,
            data: json,
            borderColor:'#2F2F30',
            dataLabels: {
                enabled: false,
                color: '#2F2F30'
            }
        }]

    });

}
