
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

    //$('#wago').val("ETC701_5A")

});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear)

    $.get('/api/year/2014/' + (currentYear), function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

    });

    $('#yearDropDown').dropdown({
        onChange: selectionPerformed,
    });

    $('#wagoDropDown').dropdown({
        onChange: selectionPerformed,
    });

    getWagosForDropDown()

    selectionPerformed()

}

function getWagosForDropDown() {

    $.get('/api/wagos/dropDown/', function (response) {
        var json = response

        var list = $("#wagoDropDownList")

        json.results.forEach(function(e) {

            list.append('<div class="item" data-value="' + e.value + '">' + e.name + '</div>')

        })

        $("#wagoDropDown").dropdown("refresh");

        $('#wagoDropDown').dropdown('set selected', json.results[0].name)

        $('#wago').val(json.results[0].value)

    })

}

function selectionPerformed() {

    var year = $('#year').val();

    var wago = $('#wago').val();

    $('#pieChartLoader').dimmer('show')

    $('#statsLoader').dimmer('show')


    $.get('/api/wagoDownTime/pieChart/'+ wago + '/' + year, function(response) {
        var json = response

        drawPieChart(json, year, wago, 'pieChart')

        $('#pieChartLoader').dimmer('hide')

    })

    $.get('/api/wagoDownTime/statistics/'+ wago + '/' + year, function(response) {
        var json = response

        $('#secondsDown').empty()
        $('#secondsDown').append(json.totalDownTimeInSeconds)

        $('#averageStop').empty()
        $('#averageStop').append(json.averageDownTime)

        $('#totalDownTimes').empty()
        $('#totalDownTimes').append(json.totalDownTimes)

        $('#statsLoader').dimmer('hide')

    })

}

function drawPieChart(json, year, wago, destinationDiv) {

    wago = wago.replace('_', '/')

    Highcharts.chart(destinationDiv, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: wago + ' availability (' + year + ')'
        },
        subtitle: {
            text: 'Source: PSEN'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.4f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.4f} %',
                    style: {
                        //color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        colors: ['#E53F3F', '#9ff48c'],

        series: json
    });

}
