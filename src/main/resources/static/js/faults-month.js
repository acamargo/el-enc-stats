/**
 * Created by alejandrocamargo on 12/3/17.
 */

// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })


});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear)

    $.get('/api/year/2009/' + (currentYear), function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });

    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });


}

function yearSelected() {

    $('#majorFaultsBarChartLoader').dimmer('show');
    $('#minorFaultsBarChartLoader').dimmer('show');

    var year = $('#year').val();

    $.get('/api/majorFaults/EN-EL/barChart/' + year, function(response) {
        var json = response

        drawBarChart(json, year, 'majorFaultsBarChart', 'Major Faults per month')
        $('#majorFaultsBarChartLoader').dimmer('hide');

    })

    $.get('/api/minorFaults/EN-EL/barChart/' + year, function(response) {
        var json = response

        drawBarChart(json, year, 'minorFaultsBarChart', 'Minor Faults per month')
        $('#minorFaultsBarChartLoader').dimmer('hide');

    })


}

function drawBarChart(json, year, destinationDiv, title) {

    Highcharts.chart(destinationDiv, {
        chart: {
            type: 'column'
        },
        title: {
            text: title + ' (' +  year + ')'
        },
        subtitle: {
            text: 'Source: OP-TI LogBook'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of faults'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: json

    });

}