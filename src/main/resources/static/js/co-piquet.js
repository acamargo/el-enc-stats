/**
 * Created by acamargo on 07/02/2017.
 */
// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    //initialize semantic-ui components
    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2010/' + currentYear, function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });

    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });

}

function yearSelected() {

    $('#eccoBarChartLoader').dimmer('show');
    $('#eccpBarChartLoader').dimmer('show');

    var year = $('#year').val();

    $.get('/api/coPiquet/ECCO/barChart/' + year, function(response) {
        var json = response

        drawBarChart(json, year, 'eccoBarChart', 'ECCO')
        $('#eccoBarChartLoader').dimmer('hide');

    })

    $.get('/api/coPiquet/ECCP/barChart/' + year, function(response) {
        var json = response

        drawBarChart(json, year, 'eccpBarChart', 'ECCP')
        $('#eccpBarChartLoader').dimmer('hide');

    })

}

function drawBarChart(json, year, destinationDiv, piquetName) {

    Highcharts.chart(destinationDiv, {
        chart: {
            type: 'column'
        },
        title: {
            text: piquetName + ' RTU Related (' + year + ')'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        xAxis: {
            categories: [
                year
            ],
            crosshair: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of interventions'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: json

    });
}