// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    //initialize semantic-ui components
    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2014/' + currentYear, function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });

    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });

}

function yearSelected() {

    $('#pieChartLoader').dimmer('show');
    $('#barChartLoader').dimmer('show');
    $('#lineChartLoader').dimmer('show');


    var year = $('#year').val();

    $.get('/api/piquetOverview/barChart/' + year, function(response) {
        var json = response

        drawBarChart(json, year)
        $('#barChartLoader').dimmer('hide');

    })

    $.get('/api/piquetOverview/pieChart/' + year, function(response) {
        var json = response

        drawPieChart(json, year)
        $('#pieChartLoader').dimmer('hide');

    })

    $.get('/api/piquetOverview/lineChart/' + year, function(response) {
        var json = response

        drawLineChart(json, year)
        $('#lineChartLoader').dimmer('hide');

    })

}

function drawBarChart(json, year) {

    Highcharts.chart('barChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Piquet Interventions (' + year + ')'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        xAxis: {
            categories: [
                year
            ],
            crosshair: false
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of interventions'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: json

    });

}

function drawPieChart(json, year) {

    Highcharts.chart('pieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '% Piquet Interventions (' + year + ')'
        },
        subtitle: {
            text: 'Source: EAM'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },

        series: json
    });

}

function drawLineChart(json, year) {

    Highcharts.chart('lineChart', {
        title: {
            text: 'Monthly Piquet Interventions (' + year + ')',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: EAM',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Number of interventions'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },

        series: json

    });

}

