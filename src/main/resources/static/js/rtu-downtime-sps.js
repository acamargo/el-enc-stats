/**
 * Created by alejandrocamargo on 15/3/17.
 */

// Main function
$(document).ready(function() {

    //include top and footer
    $("#footer").load("templates/footer.html")

    $("#topMenu").load("templates/top-menu.html", function (responseData) { initializeComponents() })

});

function initializeComponents() {

    initializeTopMenu()

    var currentYear = new Date().getFullYear();

    $('#year').val(currentYear - 1)

    $.get('/api/year/2014/' + (currentYear - 1), function(response) {

        var list = $("#yearDropDownList")

        response.forEach(function(e) {

            list.append('<div class="item" data-value="' + e + '">' + e + '</div>')

        })

        $("#yearDropDown").dropdown("refresh");

        $('#yearDropDown').dropdown('set selected', currentYear - 1)

        yearSelected()

    });


    $('#yearDropDown').dropdown({
        onChange: yearSelected,
    });


}

function yearSelected() {

    var year = $('#year').val();

    var rtus = ["ETC03_A1", "ETC03_A2", "ETC03_A3", "ETC03_A4", "ETC01_A5", "ETC03_A6", "ETC03_A7", "ETC03_A80", "ETC03_A81", "ETC03_A82", "ETC03_A85"]

    rtus.forEach(function(item, index) {

        $('#' + item.toLowerCase() + '_loader').dimmer('show')

        $.get('/api/rtuDownTime/pieChart/'+ item + '/' + year, function(response) {
            var json = response

            drawPieChart(json, year, item, item.toLowerCase())

            $('#' + item.toLowerCase() + '_loader').dimmer('hide')

        })

    })

}

function drawPieChart(json, year, rtu, destinationDiv) {

    rtu = rtu.replace('_', '/')

    Highcharts.chart(destinationDiv, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: rtu + ' availability (' + year + ')'
        },
        subtitle: {
            text: 'Source: PSEN'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.3f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.3f} %',
                    style: {
                        //color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },

        colors: ['#E53F3F', '#9ff48c'],

        series: json
    });

}