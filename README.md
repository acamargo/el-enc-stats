Application to gather and calculate statistics for the electrical group.
Some of the data sources are EAM, PSEN and OP-TI Logbook.

Using Spring Boot, Spring Data JPA, Spring JDBC, Spring MVC, Mongo DB, JQuery, Javascript, Semantic UI.
